export const navigations = [
    // {
    //     name: 'Dashboard',
    //     path: '/dashboard/default',
    //     icon: 'dashboard',
    // },

    {
        name: 'Appointments',
        path: '/appointments',
        icon: 'assignment',
    },
    {
        name: 'Members',
        path: '/members',
        icon: 'people',
    },
    {
        name: 'Finance',
        path: '/finance',
        icon: 'account_balance',
    },
    {
        name: 'Products',
        path: '/products',
        icon: 'discount',
    },
    // {
    //     name: 'Acquisition',
    //     path: '/Acquisitions',
    //     icon: 'shopping_cart',

    // },
    {
        name: 'Employees',
        path: '/employees',
        icon: 'admin_panel_settings',
    },
    // {
    //     name: 'Analytics',
    //     path: '/session/404',
    //     icon: 'equalizer',

    // },
    // {
    //     name: 'Communication',
    //     path: '/session/404',
    //     icon: 'chat',

    // },
    {
        name: 'Locations',
        path: '/locations',
        icon: 'maps_home_work',
    },
    {
        name: 'Settings',
        path: '/settings',
        icon: 'settings',
    },
]
