import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getCompanyDetails = createAsyncThunk(
    'settings/getCompanyDetails',
    async (id) => {
        const response = await axios.get(`${PREFIX}companies/${id}`)
        if (!response) {
            throw new Error("Can't get company settings.Server Error!")
        }
        return response.data
    }
)

export const changeCompanyDetails = createAsyncThunk(
    'settings/changeCompanyDetails',
    async (payload) => {
        const response = await axios.put(
            `${PREFIX}companies/${payload.id}`,
            payload.data
        )
        if (!response) {
            throw new Error("Can't change company settings. Server error.")
        }
        return response.data
    }
)

export const getMembershipSettings = createAsyncThunk(
    'settings/getMembershipSettings',
    async () => {
        const response = await axios.get(`${PREFIX}membershipSettings`)
        if (!response) {
            throw new Error("Can't get mempership settings.Server Error!")
        }
        return response.data
    }
)

export const getMembershipData = createAsyncThunk(
    'settings/getMembershipData',
    async () => {
        const response = await axios.get(`${PREFIX}membership`)
        if (!response) {
            throw new Error("Can't get mempership data.Server Error!")
        }
        return response.data
    }
)

export const changeMembership = createAsyncThunk(
    'settings/changeMembership',
    async (payload) => {
        const response = await axios.patch(
            `${PREFIX}membership/${payload.id}`,
            payload
        )
        if (!response) {
            throw new Error("Can't change mempership!")
        }
        return response.data
    }
)

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    companyInfo: [],
    membershipSettings: [],
    membershipData: [],
    status: null,
    error: null,
    isUpdated: false,
}

const SettingsSlice = createSlice({
    name: 'settings',
    initialState,
    reducers: {},
    extraReducers: {
        // Get company details extra reducers
        [getCompanyDetails.pending]: setLoading,
        [getCompanyDetails.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.isUpdated = false
            state.companyInfo = action.payload
        },
        [getCompanyDetails.rejected]: setError,

        // Change company details extra reducers
        [changeCompanyDetails.pending]: setLoading,
        [changeCompanyDetails.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.isUpdated = true
            state.companyInfo = action.payload
        },
        [changeCompanyDetails.rejected]: setError,

        // Get membership settings
        [getMembershipSettings.pending]: setLoading,
        [getMembershipSettings.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.membershipSettings = action.payload
        },
        [getMembershipSettings.rejected]: setError,

        // Get membership data
        [getMembershipData.pending]: setLoading,
        [getMembershipData.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.membershipData = action.payload
        },
        [getMembershipData.rejected]: setError,

        // Change membership
        [changeMembership.pending]: setLoading,
        [changeMembership.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.membershipData.findIndex(
                (item) => item.id === parseInt(action.payload.id)
            )
            state.membershipData[itemId] = action.payload
            state.membershipData = action.payload
        },
        [changeMembership.rejected]: setError,
    },
})

export default SettingsSlice.reducer
