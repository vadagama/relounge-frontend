import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Divider, Tabs } from '@mui/material'
import { Breadcrumb } from 'app/components'
import { MembershipSettings } from './MembershipSettings'
import { CompanyDetails } from './CompanyDetails'
import { Container } from 'app/components/styled/Container'
import { StyledTab } from 'app/components/styled/StyledTab'
import { getCompanyDetails } from './SettingsSlice'

const SettingsApp = () => {
    const [tabIndex, setTabIndex] = useState(0)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getCompanyDetails(0))
    }, [dispatch])

    const handleTabChange = (e, value) => {
        setTabIndex(value)
    }

    return (
        <Container>
            <div>
                <Breadcrumb
                    routeSegments={[{ name: 'Settings', path: '/settings' }]}
                />
            </div>
            <Tabs
                value={tabIndex}
                onChange={handleTabChange}
                indicatorColor="primary"
                textColor="primary"
            >
                {tabList.map((item, ind) => (
                    <StyledTab value={ind} label={item} key={ind} />
                ))}
            </Tabs>
            <Divider />

            {tabIndex === 0 && <CompanyDetails />}
            {tabIndex === 1 && <MembershipSettings />}
        </Container>
    )
}

const tabList = ['Company Details', 'Membership']

export default SettingsApp
