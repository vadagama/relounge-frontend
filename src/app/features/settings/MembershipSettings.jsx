import React from 'react'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { SimpleCard } from 'app/components'
import { Button, Grid } from '@mui/material'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Switch from '@mui/material/Switch'
import Paper from '@mui/material/Paper'
import Stack from '@mui/material/Stack'
import Chip from '@mui/material/Chip'
import { styled } from '@mui/system'
import CheckCircleOutline from '@mui/icons-material/CheckCircleOutline'
import { getMembershipData } from './SettingsSlice'
import { getMembershipSettings } from './SettingsSlice'
import { changeMembership } from './SettingsSlice'

export const MembershipSettings = () => {
    const [checked, setChecked] = React.useState(false)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getMembershipSettings())
        dispatch(getMembershipData())
    }, [dispatch])

    const { membershipSettings, membershipData } = useSelector(
        (state) => state.settings
    )

    console.log(membershipSettings)
    console.log(membershipData)

    const handleChange = (event) => {
        setChecked(event.target.checked)
    }

    const HeaderArea = styled(Paper)(({ theme }) => ({
        backgroundColor: '#E6EAFD',
        ...theme.typography.body2,
        paddingTop: '10px',
        paddingBottom: '20px',
        textAlign: 'center',
        color: theme.palette.text.primary,
    }))

    const HeaderAreaLeft = styled(Paper)(({ theme }) => ({
        backgroundColor: '#E6EAFD',
        ...theme.typography.body2,
        paddingTop: '10px',
        height: '100%',
        minHeight: '630px',
        textAlign: 'center',
        color: theme.palette.text.primary,
    }))
    const ContentArea = styled(Paper)(({ theme }) => ({
        backgroundColor: '#FFFFFF',
        ...theme.typography.body2,
        marginTop: '10px',
        paddingTop: '20px',
        paddingBottom: '20px',
        textAlign: 'center',
        color: theme.palette.text.primary,
    }))

    const Content = styled('div')(({ theme }) => ({
        ...theme.typography.body2,
        height: '40px',
        textAlign: 'center',
        color: theme.palette.text.primary,
    }))

    const ContentLeft = styled('div')(({ theme }) => ({
        ...theme.typography.body2,
        paddingLeft: '30px',
        variant: 'outlined',
        height: '40px',
        textAlign: 'left',
        color: theme.palette.text.primary,
    }))

    const ContentAreaLeft = styled('div')(({ theme }) => ({
        ...theme.typography.body2,
        height: '40px',
        marginTop: '30px',
        textAlign: 'left',
        color: theme.palette.text.primary,
    }))

    return (
        <>
            <SimpleCard title="Membership options">
                <Grid container spacing={1}>
                    <Grid item lg={3} md={3} sm={3} xs={12}>
                        <HeaderAreaLeft>
                            <Typography variant="h6" component="div">
                                Features
                            </Typography>
                            <Stack
                                direction="row"
                                spacing={1}
                                justifyContent="center"
                                alignItems="center"
                            >
                                <Typography>Month</Typography>
                                <Switch
                                    checked={checked}
                                    onChange={handleChange}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                                <Typography>Year</Typography>
                            </Stack>
                            {!checked ? (
                                <Box sx={{ height: '0px' }} />
                            ) : (
                                <Box sx={{ height: '24px' }} />
                            )}
                            <ContentAreaLeft>
                                {membershipSettings.map((item) => (
                                    <ContentLeft key={item.id}>
                                        {item.name}
                                    </ContentLeft>
                                ))}
                            </ContentAreaLeft>
                        </HeaderAreaLeft>
                    </Grid>
                    <Grid item lg={3} md={3} sm={3} xs={12}>
                        {!checked ? (
                            <HeaderArea>
                                <Stack spacing={0}>
                                    <Typography variant="h6">Basic</Typography>
                                    <Typography variant="h7">free</Typography>
                                </Stack>
                            </HeaderArea>
                        ) : (
                            <HeaderArea>
                                <Stack spacing={0}>
                                    <Typography variant="h6">Basic</Typography>
                                    <Typography variant="h7">free</Typography>
                                    <Box sx={{ height: '34px' }} />
                                </Stack>
                            </HeaderArea>
                        )}
                        <ContentArea>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>50</Content>
                            <Content>&mdash;</Content>
                            <Content>&mdash;</Content>
                            <Content>&mdash;</Content>
                            <Content>&mdash;</Content>
                            <Content>helpcenter</Content>
                            <Content>1 User</Content>
                            <Content>
                                <Button variant="contained" color="success">
                                    Active
                                </Button>
                            </Content>
                        </ContentArea>
                    </Grid>
                    <Grid item lg={3} md={3} sm={3} xs={12}>
                        {!checked ? (
                            <HeaderArea>
                                <Stack spacing={0}>
                                    <Typography variant="h6">
                                        Professional
                                    </Typography>
                                    <Typography variant="h7">
                                        99 € / Month
                                    </Typography>
                                </Stack>
                            </HeaderArea>
                        ) : (
                            <HeaderArea>
                                <Stack spacing={0}>
                                    <Typography variant="h6">
                                        Professional
                                    </Typography>
                                    <Typography variant="h7">
                                        1069 € / Year
                                    </Typography>
                                    <div>
                                        <Chip
                                            label="save 10%"
                                            color="success"
                                            size="small"
                                            sx={{ margin: '5px' }}
                                        />
                                    </div>
                                </Stack>
                            </HeaderArea>
                        )}
                        <ContentArea>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>150</Content>
                            <Content>&mdash;</Content>
                            <Content>&mdash;</Content>
                            <Content>yes</Content>
                            <Content>members</Content>
                            <Content>helpcenter, email</Content>
                            <Content>5+ User</Content>
                            <Content>
                                <Button variant="contained" color="primary">
                                    Upgrade
                                </Button>
                            </Content>
                        </ContentArea>
                    </Grid>
                    <Grid item lg={3} md={3} sm={3} xs={12}>
                        {!checked ? (
                            <HeaderArea>
                                <Stack spacing={0}>
                                    <Typography variant="h6">
                                        Advanced
                                    </Typography>
                                    <Typography variant="h7">
                                        149 € / Month
                                    </Typography>
                                </Stack>
                            </HeaderArea>
                        ) : (
                            <HeaderArea>
                                <Stack spacing={0}>
                                    <Typography variant="h6">
                                        Advanced
                                    </Typography>
                                    <Typography variant="h7">
                                        1430 € / Year
                                    </Typography>
                                    <div>
                                        <Chip
                                            label="save 10%"
                                            color="success"
                                            size="small"
                                            sx={{ margin: '5px' }}
                                        />
                                    </div>
                                </Stack>
                            </HeaderArea>
                        )}
                        <ContentArea>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>
                                <CheckCircleOutline color="success" />
                            </Content>
                            <Content>no limit</Content>
                            <Content>yes</Content>
                            <Content>yes</Content>
                            <Content>yes</Content>
                            <Content>members/staff</Content>
                            <Content>helpcenter, email, chat</Content>
                            <Content>10+ User</Content>
                            <Content>
                                <Button variant="contained" color="primary">
                                    Upgrade
                                </Button>
                            </Content>
                        </ContentArea>
                    </Grid>
                </Grid>
            </SimpleCard>
        </>
    )
}
