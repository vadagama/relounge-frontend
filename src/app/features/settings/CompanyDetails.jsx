import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import { SimpleCard } from 'app/components'
import Divider from '@mui/material/Divider'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import { styled } from '@mui/system'
import { changeCompanyDetails } from './SettingsSlice'

const TextField = styled(TextValidator)(() => ({
    width: '100%',
    marginBottom: '16px',
}))

export const CompanyDetails = () => {
    const dispatch = useDispatch()
    const { status, error, companyInfo } = useSelector(
        (state) => state.settings
    )
    const [state, setState] = useState()

    useEffect(() => {
        setState(companyInfo)
    }, [companyInfo])

    const handleChange = (event) => {
        event.persist()
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    const handleSubmit = (event) => {
        dispatch(changeCompanyDetails({ id: 0, data: state }))
    }

    return (
        <div>
            <SimpleCard title="Company details">
                {state && !error && status !== 'loading' && (
                    <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <TextField
                                    required
                                    name="name"
                                    label="Company Name"
                                    type="text"
                                    onChange={handleChange}
                                    value={state.name || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 50',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 4 symbols',
                                        'should be less than 50 symbols',
                                    ]}
                                />
                                <TextField
                                    required
                                    name="phone"
                                    label="Phone"
                                    type="text"
                                    onChange={handleChange}
                                    id="standard-basic"
                                    value={state.phone || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 15',
                                    ]}
                                    errorMessages={['this field is required']}
                                />
                                <TextField
                                    required
                                    label="registration Number"
                                    onChange={handleChange}
                                    name="registrationNumber"
                                    type="text"
                                    value={state.registrationNumber || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <TextField
                                    required
                                    label="Website"
                                    onChange={handleChange}
                                    type="text"
                                    name="website"
                                    value={state.website || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                                <TextField
                                    required
                                    name="email"
                                    label="Email"
                                    type="email"
                                    onChange={handleChange}
                                    id="standard-basic"
                                    value={state.email || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 30',
                                    ]}
                                    errorMessages={['this field is required']}
                                />
                                <TextField
                                    required
                                    label="VAT Number"
                                    onChange={handleChange}
                                    name="taxNumber"
                                    type="text"
                                    value={state.taxNumber || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                            </Grid>
                        </Grid>

                        <Divider>Address</Divider>
                        <Box sx={{ py: '10px' }} />
                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <TextField
                                    required
                                    label="Country"
                                    onChange={handleChange}
                                    type="text"
                                    name="country"
                                    value={state.country || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                                <TextField
                                    required
                                    label="Street"
                                    onChange={handleChange}
                                    type="text"
                                    name="street"
                                    value={state.street || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                                <TextField
                                    required
                                    label="Postal Code"
                                    onChange={handleChange}
                                    type="text"
                                    name="postalCode"
                                    value={String(state.postalCode) || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <TextField
                                    required
                                    label="City"
                                    onChange={handleChange}
                                    type="text"
                                    name="city"
                                    value={state.city || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                                <TextField
                                    required
                                    label="Building"
                                    onChange={handleChange}
                                    type="text"
                                    name="building"
                                    value={String(state.building) || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                            </Grid>
                        </Grid>
                        <Box sx={{ py: '6px' }} />
                        <Button
                            color="primary"
                            variant="contained"
                            type="submit"
                        >
                            Save Changes
                        </Button>
                    </ValidatorForm>
                )}
                {error && 'ERR_CONNECTION_REFUSED'}
                {/* {status === 'loading' && (
                    <Stack spacing={1}>
                        <Skeleton variant="rectangular" />
                        <Skeleton variant="rectangular" />
                        <Skeleton variant="rectangular" />
                    </Stack>
                )} */}
            </SimpleCard>
        </div>
    )
}
