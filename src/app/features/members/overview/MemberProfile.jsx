import React from 'react'
import { Table, TableBody, TableCell, TableRow, Box } from '@mui/material'
import { SimpleCard } from 'app/components'

const MemberProfile = (props) => {
    const member = {
        'First Name': props.selectedMember.firstName,
        'Last Name': props.selectedMember.lastName,
        Email: props.selectedMember.email,
        Phone: props.selectedMember.phone,
        Address: props.selectedMember.address,
        'Height (cm)': props.selectedMember.height,
        'Weight (kg)': props.selectedMember.weight,
    }

    return (
        <>
            <Box sx={{ py: '10px' }} />
            <SimpleCard title="General information">
                <Table>
                    <TableBody>
                        {Object.entries(member).map(([key, subject], i) => (
                            <TableRow key={i}>
                                <TableCell>{key}</TableCell>
                                <TableCell>{subject}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </SimpleCard>
        </>
    )
}

export default MemberProfile
