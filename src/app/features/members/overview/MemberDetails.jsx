import React from 'react'
import { useSelector } from 'react-redux'
import { Grid, Fade } from '@mui/material'
import MemberProfile from './MemberProfile'
import MemberMembership from './MemberMembership'

const MemberDetails = () => {
    const { selectedMember } = useSelector((state) => state.members)

    return (
        <Fade in timeout={300}>
            <Grid container spacing={2}>
                <Grid item lg={4} md={6} xs={12}>
                    <MemberProfile selectedMember={selectedMember} />
                </Grid>
                <Grid item lg={6} md={6} xs={12}>
                    <MemberMembership selectedMember={selectedMember} />
                </Grid>
            </Grid>
        </Fade>
    )
}

export default MemberDetails
