import { Table, TableBody, TableCell, TableRow, Box } from '@mui/material'
import React, { useState } from 'react'
import { SimpleCard } from 'app/components'
import CardActions from '@mui/material/CardActions'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import { AlertDialog } from '../AlertDialog'

const MemberMembership = (props) => {
    const [dialogIsOpen, setDialogIsOpen] = useState(false)

    const openDialog = (event) => {
        event.preventDefault()
        setDialogIsOpen(true)
    }

    const closeDialog = (event) => {
        event.preventDefault()
        setDialogIsOpen(false)
    }

    const cancelMembershipDialog = async () => {
        // Call API method
        setDialogIsOpen(false)
    }

    const member = {
        'Number of trainings': props.selectedMember.trainingsTotal,
        'Start date': props.selectedMember.startDate,
        'End date': props.selectedMember.endDate,
        'Extension date': props.selectedMember.extensionDate,
    }

    return (
        <>
            <Box sx={{ py: '10px' }} />
            <SimpleCard title="Membership details">
                <CardMedia
                    component="img"
                    alt="green iguana"
                    height="140"
                    image="https://s3.us-east-1.amazonaws.com/iida-production-assets/Member2022Header.jpg"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Monthly membership
                    </Typography>
                    <Table>
                        <TableBody>
                            {Object.entries(member).map(([key, subject], i) => (
                                <TableRow key={i}>
                                    <TableCell>{key}</TableCell>
                                    <TableCell>{subject}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </CardContent>
                <CardActions>
                    <Button
                        variant="outlined"
                        color="error"
                        size="small"
                        onClick={openDialog}
                    >
                        Cancel membership
                    </Button>
                </CardActions>
            </SimpleCard>
            <AlertDialog
                open={dialogIsOpen}
                onCancel={cancelMembershipDialog}
                onClose={closeDialog}
            />
        </>
    )
}

export default MemberMembership
