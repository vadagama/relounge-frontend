import React, { lazy } from 'react'
import Loadable from 'app/components/Loadable/Loadable'
import { authRoles } from '../../auth/authRoles'

const MembersList = Loadable(lazy(() => import('./MembersList')))
const MembersApp = Loadable(lazy(() => import('./MembersApp')))

const membersRoutes = [
    {
        path: '/members',
        element: <MembersList />,
        auth: authRoles.admin,
    },
    {
        path: '/members/:id',
        element: <MembersApp />,
        auth: authRoles.admin,
    },
]

export default membersRoutes
