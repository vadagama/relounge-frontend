import * as React from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'

export const AlertDialog = (props) => {
    const { open, onClose, onCancel } = props

    return (
        <div>
            <Dialog open={open} onClose={onClose}>
                <DialogTitle>Cancel membership</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Are you sure to cancel membership?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        color="primary"
                        variant="outlined"
                        onClick={onClose}
                    >
                        No
                    </Button>
                    <Button
                        color="error"
                        variant="outlined"
                        onClick={onCancel}
                        autoFocus
                    >
                        Yes i'm sure
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}
