import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { Grid, Fade } from '@mui/material'
import Button from '@mui/material/Button'
import FinanceInvoices from './FinanceInvoices'
import FinancePayments from './FinancePayments'
import Box from '@mui/material/Box'
import SettingsIcon from '@mui/icons-material/Settings'
import { InvoiceTemplateDialog } from './InvoiceTemplateDialog'
import { getAllInvoices } from 'app/features/finance/FinanceSlice'
import { getAllPayments } from 'app/features/finance/FinanceSlice'
import { getInvoiceTemplate } from 'app/features/finance/FinanceSlice'
import { changeInvoiceTemplate } from 'app/features/finance/FinanceSlice'

const FinanceDetails = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllInvoices())
        dispatch(getAllPayments())
        dispatch(getInvoiceTemplate())
    }, [dispatch])

    const { invoiceTemplate } = useSelector((state) => state.finance)

    const [dialogIsOpen, setDialogIsOpen] = useState(false)

    const openDialog = (event) => {
        event.preventDefault()
        setDialogIsOpen(true)
    }

    const closeDialog = (event) => {
        event.preventDefault()
        setDialogIsOpen(false)
    }

    const saveInvoiceTemplateDialog = async (state) => {
        dispatch(changeInvoiceTemplate(state))
        setDialogIsOpen(false)
    }

    return (
        <Fade in timeout={200}>
            <div>
                <Box sx={{ py: '10px' }} />
                <Grid container spacing={2} justifyContent="flex-end">
                    <Grid item lg={12} md={12} xs={12}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={openDialog}
                            startIcon={<SettingsIcon />}
                        >
                            Invoice template settings
                        </Button>
                    </Grid>
                    <Grid item lg={6} md={12} xs={12}>
                        <FinanceInvoices />
                    </Grid>
                    <Grid item lg={6} md={6} xs={12}>
                        <FinancePayments />
                    </Grid>
                </Grid>
                {invoiceTemplate && (
                    <InvoiceTemplateDialog
                        open={dialogIsOpen}
                        onSave={saveInvoiceTemplateDialog}
                        onClose={closeDialog}
                        invoiceTemplate={invoiceTemplate}
                    />
                )}
            </div>
        </Fade>
    )
}

export default FinanceDetails
