import * as React from 'react'
import { useSelector } from 'react-redux'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TablePagination from '@mui/material/TablePagination'
import { SimpleCard } from 'app/components'

const FinancePayments = () => {
    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value)
        setPage(0)
    }

    const { payments } = useSelector((state) => state.finance)

    return (
        <>
            <SimpleCard title="Payments">
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{ width: '130px' }}>
                                    Service Name
                                </TableCell>
                                <TableCell>Payment Date</TableCell>
                                <TableCell>Invoice No</TableCell>
                                <TableCell>Amount</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {payments
                                .slice(
                                    page * rowsPerPage,
                                    page * rowsPerPage + rowsPerPage
                                )
                                .map((row) => (
                                    <TableRow
                                        sx={{ height: '65px' }}
                                        key={row.id}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.serviceName}
                                        </TableCell>
                                        <TableCell>{row.date}</TableCell>
                                        <TableCell>
                                            {row.invoiceNumber}
                                        </TableCell>
                                        <TableCell>$ {row.amount}</TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 25, 100]}
                    component="div"
                    count={payments.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </SimpleCard>
        </>
    )
}

export default FinancePayments
