import * as React from 'react'
import { useSelector } from 'react-redux'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TablePagination from '@mui/material/TablePagination'
import IconButton from '@mui/material/IconButton'
import Tooltip from '@mui/material/Tooltip'
import DownloadIcon from '@mui/icons-material/Download'
import { SimpleCard } from 'app/components'

const FinanceInvoices = () => {
    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value)
        setPage(0)
    }

    const { invoices } = useSelector((state) => state.finance)

    return (
        <>
            <SimpleCard title="Invoices">
                <TableContainer>
                    <Table size="small" aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{ width: '130px' }}>
                                    Invoice number
                                </TableCell>
                                <TableCell>Date</TableCell>
                                <TableCell>Amount</TableCell>
                                <TableCell>Status</TableCell>
                                <TableCell sx={{ width: '50px' }}></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {invoices
                                .slice(
                                    page * rowsPerPage,
                                    page * rowsPerPage + rowsPerPage
                                )
                                .map((row) => (
                                    <TableRow key={row.id}>
                                        <TableCell component="th" scope="row">
                                            {row.number}
                                        </TableCell>
                                        <TableCell>{row.date}</TableCell>
                                        <TableCell>$ {row.amount}</TableCell>
                                        <TableCell>{row.status}</TableCell>
                                        <TableCell>
                                            <a
                                                href={row.file}
                                                target="_blank"
                                                rel="noreferrer"
                                            >
                                                <IconButton>
                                                    <Tooltip title="Download">
                                                        <DownloadIcon />
                                                    </Tooltip>
                                                </IconButton>
                                            </a>
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={invoices.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </SimpleCard>
        </>
    )
}

export default FinanceInvoices
