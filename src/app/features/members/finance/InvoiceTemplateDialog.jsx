import * as React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import { MembersSlice } from '../MembersSlice'

export const InvoiceTemplateDialog = ({
    open,
    onSave,
    onClose,
    invoiceTemplate,
}) => {
    const [state, setState] = useState()

    useEffect(() => {
        setState(invoiceTemplate)
    }, [invoiceTemplate])

    const handleChange = (event) => {
        event.persist()
        console.log(event.target.value)
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    return (
        <div>
            {state && (
                <Dialog open={open} onClose={onClose}>
                    <DialogTitle>Invoice Template Settings</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item lg={12} md={12} xs={12}>
                                <Typography variant="subtitle1" color="initial">
                                    General information
                                </Typography>
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="First name"
                                    name="firstName"
                                    value={state.firstName}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="Email"
                                    name="email"
                                    value={state.email}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="Last name"
                                    name="lastName"
                                    value={state.lastName}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="Company name"
                                    name="companyName"
                                    value={state.companyName}
                                    onChange={handleChange}
                                />
                            </Grid>
                        </Grid>
                        <Box sx={{ py: '10px' }} />
                        <Grid container spacing={2}>
                            <Grid item lg={12} md={12} xs={12}>
                                <Typography variant="subtitle1" color="initial">
                                    Location
                                </Typography>
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="Country"
                                    name="country"
                                    value={state.country}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="Postal code"
                                    name="postalCode"
                                    value={state.postalCode}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="City"
                                    name="city"
                                    value={state.city}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} xs={12}>
                                <TextField
                                    required
                                    sx={{ width: '100%' }}
                                    label="Street/No"
                                    name="street"
                                    value={state.street}
                                    onChange={handleChange}
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            color="primary"
                            variant="outlined"
                            onClick={onClose}
                        >
                            No
                        </Button>
                        <Button
                            color="error"
                            variant="outlined"
                            onClick={() => onSave(state)}
                            autoFocus
                        >
                            Yes i'm sure
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        </div>
    )
}
