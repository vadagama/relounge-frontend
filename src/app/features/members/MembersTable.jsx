import {
    IconButton,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Tooltip,
    TablePagination,
    Button,
} from '@mui/material'
import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import DeleteIcon from '@mui/icons-material/Delete'
import VisibilityIcon from '@mui/icons-material/Visibility'
import { StyledTable } from 'app/components/styled/StyledTable'
import SentimentSatisfiedAlt from '@mui/icons-material/SentimentSatisfiedAlt'
import DeleteDialog from './DeleteDialog'
import { deleteMemberById } from './MembersSlice'

export default function MembersTable(props) {
    const dispatch = useDispatch()
    const [rowsPerPage, setRowsPerPage] = React.useState(5)
    const [page, setPage] = React.useState(0)
    const [dialogIsOpen, setDialogIsOpen] = useState(false)
    const [currentItem, setItem] = useState()
    let navigate = useNavigate()

    const { members, error } = useSelector((state) => state.members)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value)
        setPage(0)
    }

    const handleViewClick = (event) => {
        event.preventDefault()
        navigate(`${event.currentTarget.value}`, { replace: true })
    }

    const handeDeleteClick = (event) => {
        event.preventDefault()
        setItem(event.currentTarget.value)
        openDialog()
    }

    const openDialog = () => setDialogIsOpen(true)
    const closeDialog = () => setDialogIsOpen(false)

    const deleteMemberDialog = () => {
        try {
            dispatch(deleteMemberById(currentItem))

            if (error) {
                console.log(error)
            } else {
                // console.log(replyMessage)
                setDialogIsOpen(false)
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <>
            <StyledTable>
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>Trainings</TableCell>
                        <TableCell>Next Recheck</TableCell>
                        <TableCell>Reuse Quotient</TableCell>
                        <TableCell>Well-being</TableCell>
                        <TableCell width={100}>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {members
                        .slice(
                            page * rowsPerPage,
                            page * rowsPerPage + rowsPerPage
                        )
                        .map((member, index) => (
                            <TableRow key={member.id}>
                                <TableCell align="left">
                                    <div>
                                        <Button
                                            value={member.id}
                                            onClick={handleViewClick}
                                        >
                                            {member.firstName} {member.lastName}
                                        </Button>
                                    </div>
                                </TableCell>
                                <TableCell>
                                    <span>
                                        {' '}
                                        {member.trainingsDone} /{' '}
                                        {member.trainingsTotal}
                                    </span>
                                </TableCell>
                                <TableCell align="left">
                                    {member.nextRecheck}
                                </TableCell>
                                <TableCell> {member.reuseQuotient}</TableCell>
                                <TableCell>
                                    <IconButton>
                                        <SentimentSatisfiedAlt color="success" />
                                    </IconButton>
                                </TableCell>
                                <TableCell>
                                    <IconButton
                                        value={member.id}
                                        onClick={handleViewClick}
                                    >
                                        <Tooltip title="View">
                                            <VisibilityIcon />
                                        </Tooltip>
                                    </IconButton>
                                    <IconButton
                                        value={member.id}
                                        onClick={handeDeleteClick}
                                    >
                                        <Tooltip title="Delete">
                                            <DeleteIcon />
                                        </Tooltip>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </StyledTable>

            <TablePagination
                sx={{ px: 2 }}
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={members.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page',
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
            <DeleteDialog
                open={dialogIsOpen}
                onDelete={deleteMemberDialog}
                onClose={closeDialog}
                value={currentItem}
            />
        </>
    )
}
