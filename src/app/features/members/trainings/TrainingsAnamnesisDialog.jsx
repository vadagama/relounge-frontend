import React from 'react'
import { useSelector } from 'react-redux'
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'

export const TrainingsAnamnesisDialog = (props) => {
    const { open, onClose, dialogType } = props

    const { anamnesis, singleRecheck } = useSelector((state) => state.members)

    return (
        <>
            <Dialog open={open} onClose={onClose}>
                <DialogTitle>
                    {dialogType === 'anamnesis' ? 'Anamnesis' : 'Recheck'}
                </DialogTitle>
                <DialogContent>
                    <TableContainer>
                        <Table size="small" aria-label="simple table">
                            {dialogType === 'anamnesis' ? (
                                <TableHead></TableHead>
                            ) : (
                                <TableHead>
                                    <TableRow>
                                        <TableCell>
                                            <span>Question</span>
                                        </TableCell>
                                        <TableCell width={50}>
                                            <span>Answer</span>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                            )}
                            <TableBody>
                                {dialogType === 'anamnesis'
                                    ? anamnesis.map((object, id) => (
                                          <TableRow key={id}>
                                              <TableCell>
                                                  - {Object.keys(object)}
                                                  <br />-{' '}
                                                  {Object.values(object)}
                                              </TableCell>
                                          </TableRow>
                                      ))
                                    : singleRecheck.map((object, id) => (
                                          <TableRow key={id}>
                                              <TableCell>
                                                  {Object.keys(object)}
                                              </TableCell>
                                              <TableCell>
                                                  {Object.values(object)}
                                              </TableCell>
                                          </TableRow>
                                      ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </DialogContent>
            </Dialog>
        </>
    )
}
