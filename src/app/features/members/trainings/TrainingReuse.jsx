import React, { useState } from 'react'
import { SimpleCard } from 'app/components'
import Avatar from '@mui/material/Avatar'
import Grid from '@mui/material/Grid'
import { TrainingsGraphDialog } from './TrainingsGraphDialog'
import Button from '@mui/material/Button'

const TrainingReuse = () => {
    const [dialogIsOpen, setDialogIsOpen] = useState(false)

    const handleClick = (event) => {
        event.preventDefault()
        openDialog()
    }

    const openDialog = () => setDialogIsOpen(true)
    const closeDialog = () => setDialogIsOpen(false)

    return (
        <SimpleCard title="Reuse quotient">
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '100px' }}
            >
                <Grid item xs={3}>
                    <Button onClick={handleClick}>
                        <Avatar
                            sx={{
                                bgcolor: '#52AA5E',
                                width: '60px',
                                height: '60px',
                                alignContent: 'center',
                            }}
                        >
                            <strong>0.95</strong>
                        </Avatar>
                    </Button>
                </Grid>
            </Grid>
            <TrainingsGraphDialog open={dialogIsOpen} onClose={closeDialog} />
        </SimpleCard>
    )
}

export default TrainingReuse
