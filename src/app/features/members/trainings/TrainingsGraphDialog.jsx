import React from 'react'
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import { Column } from '@ant-design/plots'

export const TrainingsGraphDialog = (props) => {
    const { open, onClose } = props
    const data = [
        {
            week: '1w',
            trainings: 1,
        },
        {
            week: '2w',
            trainings: 0,
        },
        {
            week: '3w',
            trainings: 3,
        },
        {
            week: '4w',
            trainings: 3,
        },
        {
            week: '5w',
            trainings: 1,
        },
        {
            week: '6w',
            trainings: 2,
        },
        {
            week: '7w',
            trainings: 3,
        },
        {
            week: '8w',
            trainings: 6,
        },
        {
            week: '9w',
            trainings: 0,
        },
        {
            week: '10w',
            trainings: 1,
        },
        {
            week: '11w',
            trainings: 2,
        },
    ]
    const config = {
        data,
        height: 300,
        width: 500,
        autoFit: true,
        padding: 'auto',
        color: '#673ab7',
        xField: 'week',
        yField: 'trainings',
        label: {
            position: 'middle',
            style: {
                fill: '#FFFFFF',
                opacity: 0.6,
            },
        },
        xAxis: {
            label: {
                autoHide: true,
                autoRotate: false,
            },
        },
    }

    return (
        <>
            <Dialog open={open} onClose={onClose}>
                <DialogTitle>Trainings graph</DialogTitle>
                <DialogContent>
                    <div style={{ height: '300px', width: '500px' }}>
                        <Column {...config} />
                    </div>
                </DialogContent>
            </Dialog>
        </>
    )
}
