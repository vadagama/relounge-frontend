import * as React from 'react'
import { useSelector } from 'react-redux'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import { SimpleCard } from 'app/components'

const TrainingHistory = () => {
    const { appointmentHistory } = useSelector((state) => state.members)

    return (
        <>
            <SimpleCard title="Appointment history">
                <TableContainer sx={{ maxHeight: 290 }}>
                    <Table size="small" aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Appointmets</TableCell>
                                <TableCell>Date time</TableCell>
                                <TableCell>Status</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {appointmentHistory.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell>{row.appointment}</TableCell>
                                    <TableCell>{row.dateTime}</TableCell>
                                    <TableCell>{row.status}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </SimpleCard>
        </>
    )
}

export default TrainingHistory
