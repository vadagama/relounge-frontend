import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import Button from '@mui/material/Button'
import { SimpleCard } from 'app/components'
import LinkIcon from '@mui/icons-material/Link'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import { H4 } from 'app/components/Typography'
import { TrainingsAnamnesisDialog } from './TrainingsAnamnesisDialog'

const TrainingAnamnesis = () => {
    const [dialogType, setDialogType] = useState('')
    const [dialogIsOpen, setDialogIsOpen] = useState(false)

    const { rechecks } = useSelector((state) => state.members)

    const handleOpenAnamnesis = (event) => {
        event.preventDefault()
        setDialogType('anamnesis')
        openDialog()
    }

    const handleOpenRecheck = (event) => {
        event.preventDefault()
        setDialogType('recheck')
        openDialog()
    }

    const openDialog = () => setDialogIsOpen(true)
    const closeDialog = () => setDialogIsOpen(false)

    return (
        <SimpleCard title="Anamnesis">
            <Button
                variant="outlined"
                color="primary"
                onClick={handleOpenAnamnesis}
                startIcon={<LinkIcon />}
            >
                Link to anamnesis
            </Button>
            <H4 sx={{ padding: '20px 0 10px 0' }}>Recheck dates</H4>
            <List
                sx={{
                    width: '100%',
                    bgcolor: 'background.paper',
                    position: 'relative',
                    overflow: 'auto',
                    maxHeight: 200,
                }}
            >
                {rechecks.map((date, id) => (
                    <ListItem key={`section-${id}`} sx={{ padding: '0' }}>
                        <Button key={id} onClick={handleOpenRecheck}>
                            {date}
                        </Button>
                    </ListItem>
                ))}
            </List>
            <TrainingsAnamnesisDialog
                open={dialogIsOpen}
                onClose={closeDialog}
                dialogType={dialogType}
            />
        </SimpleCard>
    )
}

export default TrainingAnamnesis
