import React, { useEffect } from 'react'
import OutlinedInput from '@mui/material/OutlinedInput'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import ListItemText from '@mui/material/ListItemText'
import Select from '@mui/material/Select'
import Checkbox from '@mui/material/Checkbox'

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
}

export const SelectFilter = (props) => {
    const [markedFilter, setMarkedFilter] = React.useState(['Health Status'])

    const handleChange = (event) => {
        const {
            target: { value },
        } = event
        setMarkedFilter(typeof value === 'string' ? value.split(',') : value)
    }

    useEffect(() => {
        props.setMarkedCategories(markedFilter)
    }, [markedFilter, props])

    return (
        <div>
            <FormControl sx={{ marginBottom: 1, width: 300 }} size="small">
                <InputLabel id="demo-multiple-checkbox-label">
                    Analytics parameters
                </InputLabel>
                <Select
                    labelId="demo-multiple-checkbox-label"
                    id="demo-multiple-checkbox"
                    multiple
                    value={markedFilter}
                    onChange={handleChange}
                    input={<OutlinedInput label="Analytics arameters" />}
                    renderValue={(selected) => selected.join(', ')}
                    MenuProps={MenuProps}
                >
                    {props.categories.map((name) => (
                        <MenuItem key={name} value={name}>
                            <Checkbox
                                checked={markedFilter.indexOf(name) > -1}
                            />
                            <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    )
}
