import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Grid, Fade } from '@mui/material'
import TrainingAnalitics from './TrainingAnalitics'
import TrainingAnamnesis from './TrainingAnamnesis'
import TrainingHistory from './TrainingHistory'
import TrainingReuse from './TrainingReuse'
import TrainingWellbeing from './TrainingWellbeing'
import Box from '@mui/material/Box'
import { getAppointmentHistory } from '../MembersSlice'
import { getAnamnesis } from '../MembersSlice'
import { getRechecks } from '../MembersSlice'
import { getSingleRecheck } from '../MembersSlice'
import { getTrainingAnalytics } from '../MembersSlice'

const TrainingDetails = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAppointmentHistory())
        dispatch(getAnamnesis())
        dispatch(getRechecks())
        dispatch(getSingleRecheck())
        dispatch(getTrainingAnalytics())
    }, [dispatch])

    return (
        <Fade in timeout={200}>
            <div>
                <Grid container spacing={2}>
                    <Grid item lg={12} md={12} xs={12}>
                        <TrainingAnalitics />
                    </Grid>
                </Grid>
                <Box sx={{ py: '20px' }} />
                <Grid container spacing={2}>
                    <Grid item lg={6} md={6} xs={12}>
                        <TrainingHistory />
                    </Grid>
                    <Grid item lg={3} md={6} xs={12}>
                        <Grid item lg={12} md={12} xs={12}>
                            <TrainingReuse />
                        </Grid>
                        <Box sx={{ py: '10px' }} />
                        <Grid item lg={12} md={12} xs={12}>
                            <TrainingWellbeing />
                        </Grid>
                    </Grid>
                    <Grid item lg={3} md={6} xs={12}>
                        <TrainingAnamnesis />
                    </Grid>
                </Grid>
            </div>
        </Fade>
    )
}

export default TrainingDetails
