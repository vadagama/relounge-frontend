import React from 'react'
import { SimpleCard } from 'app/components'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import SentimentSatisfiedAlt from '@mui/icons-material/SentimentSatisfiedAlt'

const TrainingWellbeing = () => {
    return (
        <SimpleCard title="Wellbeing">
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '100px' }}
            >
                <Grid item xs={3}>
                    <Button>
                        <SentimentSatisfiedAlt
                            sx={{
                                color: '#52AA5E',
                                width: '70px',
                                height: '70px',
                                alignContent: 'center',
                            }}
                        />
                    </Button>
                </Grid>
            </Grid>
        </SimpleCard>
    )
}

export default TrainingWellbeing
