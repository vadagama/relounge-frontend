import React, { useState, useEffect, useRef, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { Line } from '@ant-design/plots'
import { SimpleCard } from 'app/components'
import { SelectFilter } from './SelectFilter'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import FormLabel from '@mui/material/FormLabel'
import FormControl from '@mui/material/FormControl'
import FormGroup from '@mui/material/FormGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormHelperText from '@mui/material/FormHelperText'
import Checkbox from '@mui/material/Checkbox'

const TrainingAnalitics = () => {
    const [data, setData] = useState([])
    const [categories, setCategories] = useState()
    const [markedCategories, setMarkedCategories] = useState([])
    const ref = useRef()
    let categoriesStatus = {}
    const config = {
        data,
        smooth: false,
        autoFit: true,
        height: 230,
        padding: 'auto',
        xField: 'date',
        yField: 'value',
        seriesField: 'category',
        xAxis: {
            type: 'time',
        },
        yAxis: {
            min: 0,
            max: 10,
        },
        slider: {},
        legend: {
            layout: 'vertical',
            position: 'right',
            selected: categories,
            flipPage: false,
            maxWidth: 0.1,
            maxHeight: 0.1,
        },
    }

    const { trainingAnalytics, analyticsCategories } = useSelector(
        (state) => state.members
    )

    const handleChange = (event) => {
        console.log(event.target.name)
        console.log(event.target.checked)
        setCategories({
            ...categories,
            [event.target.name]: event.target.checked,
        })
    }

    useEffect(() => {
        setData(trainingAnalytics)
    }, [trainingAnalytics])

    useEffect(() => {
        setCategories(analyticsCategories)
    }, [analyticsCategories])

    useEffect(() => {
        ref.current?.update({
            ...config,
            legend: {
                selected: categories,
            },
        })
    }, [markedCategories, categories, categoriesStatus, config])

    console.log(categories)

    return (
        <>
            <Box sx={{ py: '10px' }} />
            {/* <Stack direction="column" alignItems="flex-end">
                <SelectFilter
                    categories={categories}
                    setMarkedCategories={setMarkedCategories}
                />
            </Stack> */}
            <SimpleCard title="Training analytics">
                <Grid
                    container
                    spacing={1}
                    justifyContent="flex-end"
                    alignItems="center"
                >
                    <Grid item lg={10} md={6} xs={12}>
                        <div style={{ height: '230px', width: '100%' }}>
                            <Line
                                {...config}
                                onReady={(plot) => {
                                    ref.current = plot
                                }}
                            />
                        </div>
                    </Grid>
                    <Grid item lg={2} md={6} xs={12}>
                        <FormControl
                            sx={{
                                ml: '20px',
                                height: '220px',
                                width: '100%',
                                overflowY: 'auto',
                            }}
                            component="fieldset"
                            variant="standard"
                        >
                            <FormGroup>
                                {categories &&
                                    Object.entries(categories).map(
                                        ([key, value]) => (
                                            <div key={key}>
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={value}
                                                            onChange={
                                                                handleChange
                                                            }
                                                            name={key}
                                                        />
                                                    }
                                                    label={key}
                                                />
                                            </div>
                                        )
                                    )}
                            </FormGroup>
                        </FormControl>
                    </Grid>
                </Grid>
            </SimpleCard>
        </>
    )
}

export default TrainingAnalitics
