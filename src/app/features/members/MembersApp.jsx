import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import { Divider, Tabs } from '@mui/material'
import { Breadcrumb } from 'app/components'
import { Container } from 'app/components/styled/Container'
import { StyledTab } from 'app/components/styled/StyledTab'
import MemberDetails from './overview/MemberDetails'
import FinanceDetails from './finance/FinanceDetails'
import TrainingDetails from './trainings/TrainingDetails'
import BackcheckDetails from './backcheck/BackcheckDetails'
import { getMemberById } from './MembersSlice'

const MembersApp = () => {
    const [tabIndex, setTabIndex] = useState(0)
    const dispatch = useDispatch()
    const params = useParams()

    const handleTabChange = (e, value) => {
        setTabIndex(value)
    }

    useEffect(() => {
        dispatch(getMemberById(params.id))
    }, [dispatch, params.id])

    return (
        <Container>
            <Breadcrumb
                routeSegments={[
                    { name: 'Members', path: '/members' },
                    { name: 'Member profile', path: '/members/edit' },
                ]}
            />
            <Tabs
                value={tabIndex}
                onChange={handleTabChange}
                indicatorColor="primary"
                textColor="primary"
            >
                {tabList.map((item, ind) => (
                    <StyledTab value={ind} label={item} key={ind} />
                ))}
            </Tabs>
            <Divider />
            {tabIndex === 0 && <MemberDetails />}
            {tabIndex === 1 && <FinanceDetails />}
            {tabIndex === 2 && <TrainingDetails />}
            {tabIndex === 3 && <BackcheckDetails />}
        </Container>
    )
}

const tabList = ['Overview', 'Finance', 'Trainings', 'Backchecks']

export default MembersApp
