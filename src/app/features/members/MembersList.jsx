import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Breadcrumb, SimpleCard } from 'app/components'
import { Box } from '@mui/system'
import Stack from '@mui/material/Stack'
import IconButton from '@mui/material/IconButton'
import FilterIcon from '@mui/icons-material/FilterList'
import { Container } from 'app/components/styled/Container'
import MembersTable from './MembersTable'
import { getAllMembers, searchMembers } from './MembersSlice'
import { MembersFilters } from 'app/components/MembersFilters/MembersFilters'

const MembersList = () => {
    const dispatch = useDispatch()
    const [isVisible, setVisible] = React.useState(false)

    useEffect(() => {
        dispatch(getAllMembers())
    }, [dispatch])

    const handleShowFilter = () => {
        setVisible(true)
    }

    return (
        <div>
            <Container>
                <div className="breadcrumb">
                    <Breadcrumb
                        routeSegments={[{ name: 'Members', path: '/members' }]}
                    />
                </div>

                <SimpleCard title="Members">
                    {isVisible ? (
                        <div>
                            <MembersFilters
                                setVisible={setVisible}
                                isVisible={isVisible}
                                searchItems={(value) => {
                                    dispatch(searchMembers(value))
                                }}
                                getAllItems={() => {
                                    dispatch(getAllMembers())
                                }}
                            />
                        </div>
                    ) : (
                        <Stack direction="row" justifyContent="flex-end">
                            <IconButton
                                onClick={handleShowFilter}
                                edge="end"
                                sx={{
                                    width: 50,
                                    height: 50,
                                    position: 'absolute',
                                    marginRight: '5px',
                                    marginTop: '-50px',
                                }}
                            >
                                <FilterIcon />
                            </IconButton>
                        </Stack>
                    )}
                    <Box width="100%" overflow="auto">
                        <MembersTable />
                    </Box>
                </SimpleCard>
            </Container>
        </div>
    )
}

export default MembersList
