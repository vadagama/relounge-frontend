import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Grid, Button } from '@mui/material'
import Card from '@mui/material/Card'
import CardMedia from '@mui/material/CardMedia'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import Stack from '@mui/material/Stack'
import TextField from '@mui/material/TextField'
import { getBackchecks } from '../MembersSlice'

const BackcheckCard = (props) => {
    const [open, setOpen] = React.useState(false)

    const handleClickOpen = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <>
            {/* <Button onClick={handleClickOpen} sx={{ textAlign: 'left' }}> */}
            <Card
                sx={{ display: 'flex', cursor: 'pointer' }}
                onClick={handleClickOpen}
            >
                <Box sx={{ width: '100%' }}>
                    <Stack
                        direction="column"
                        justifyContent="flex-start"
                        alignItems="flex-start"
                        spacing={1}
                        sx={{
                            padding: '20px 0 20px 20px',
                            margin: 'auto',
                        }}
                    >
                        <Typography component="div" variant="h6">
                            {props.type}
                        </Typography>
                        <Typography component="div" variant="subtitle2">
                            {props.date}
                        </Typography>
                        <Typography
                            component="div"
                            variant="body2"
                            sx={{ width: '100%', maxHeight: '100px' }}
                        >
                            {props.description === ''
                                ? 'No comment yet'
                                : props.description}
                        </Typography>
                    </Stack>
                </Box>
                <CardMedia
                    component="img"
                    image={props.url}
                    alt="Live from space album cover"
                    sx={{ maxHeight: '200px' }}
                />
            </Card>

            <BackcheckPopup
                handleClose={handleClose}
                open={open}
                date={props.date}
                url={props.url}
            />
        </>
    )
}

const BackcheckDetails = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getBackchecks())
    }, [dispatch])

    const { backchecks } = useSelector((state) => state.members)
    return (
        <>
            <Box sx={{ py: '10px' }} />
            <Grid container spacing={2}>
                {backchecks.map((card) => (
                    <Grid item lg={4} md={6} xs={12} key={card.id}>
                        <BackcheckCard
                            date={card.date}
                            url={card.url}
                            type={card.type}
                            description={card.description}
                        />
                    </Grid>
                ))}
            </Grid>
        </>
    )
}

const BackcheckPopup = (props) => {
    return (
        <Dialog
            open={props.open}
            keepMounted
            onClose={props.handleClose}
            aria-describedby="alert-dialog-slide-description"
            sx={{ padding: '10px' }}
        >
            <DialogTitle>{'Edit a backcheck'}</DialogTitle>
            <DialogContent sx={{ padding: '20px 20px 0 20px' }}>
                <Stack direction="row" spacing={2} justifyContent="flex-end">
                    <CardMedia
                        component="img"
                        sx={{ padding: '10px', width: '165px' }}
                        image={props.url}
                        alt="Live from space album cover"
                    />
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            width: '300px',
                            paddingTop: '10px',
                        }}
                    >
                        <Stack direction="column" spacing={2}>
                            <TextField
                                label="Note to backcheck on"
                                id="filled-size-small"
                                value={props.date}
                                size="small"
                            />
                            <TextField
                                required
                                id="outlined-required"
                                label="Required"
                                size="small"
                                multiline
                                rows={4}
                            />
                        </Stack>
                    </Box>
                </Stack>
            </DialogContent>
            <DialogActions sx={{ padding: '20px' }}>
                <Button
                    variant="contained"
                    color="error"
                    onClick={props.handleClose}
                >
                    Cancel
                </Button>
                <Button
                    variant="contained"
                    color="success"
                    onClick={props.handleClose}
                >
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default BackcheckDetails
