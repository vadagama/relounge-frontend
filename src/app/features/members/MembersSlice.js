import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getAllMembers = createAsyncThunk(
    'members/getAllMembers',
    async () => {
        const response = await axios.get(`${PREFIX}members`)
        return response.data
    }
)

export const getMemberById = createAsyncThunk(
    'members/getMemberById',
    async (id) => {
        const response = await axios.get(`${PREFIX}members/${id}`)
        return response.data
    }
)

export const searchMembers = createAsyncThunk(
    'members/searchMembers',
    async (payload) => {
        const response = await axios.get(`${PREFIX}members?q=${payload}`)
        if (!response) {
            throw new Error("Server Error! Can't get members.")
        }
        return response.data
    }
)

export const createMember = createAsyncThunk(
    'members/createMember',
    async (payload) => {
        const response = await axios.post(`${PREFIX}members`, payload)
        return response.data
    }
)

export const changeMemberById = createAsyncThunk(
    'members/changeMemberById',
    async (payload) => {
        console.log(payload)
        const response = await axios.put(
            `${PREFIX}members/${payload.id}`,
            payload
        )
        return response.data
    }
)

export const deleteMemberById = createAsyncThunk(
    'members/deleteMemberById',
    async (id) => {
        const response = await axios.delete(`${PREFIX}members/${id}`)
        return response.data
    }
)

export const getAppointmentHistory = createAsyncThunk(
    'members/getAppointmentHistory',
    async () => {
        const response = await axios.get(`${PREFIX}appointmentHistory`)
        return response.data
    }
)

export const getAnamnesis = createAsyncThunk(
    'members/getAnamnesis',
    async () => {
        const response = await axios.get(`${PREFIX}anamnesis`)
        return response.data
    }
)

export const getRechecks = createAsyncThunk('members/getRechecks', async () => {
    const response = await axios.get(`${PREFIX}rechecks`)
    return response.data
})

export const getSingleRecheck = createAsyncThunk(
    'members/getSingleRecheck',
    async () => {
        const response = await axios.get(`${PREFIX}singleRecheck`)
        return response.data
    }
)

export const getTrainingAnalytics = createAsyncThunk(
    'members/getTrainingAnalytics',
    async () => {
        const response = await axios.get(`${PREFIX}trainingAnalytics`)
        return response.data
    }
)

export const getBackchecks = createAsyncThunk(
    'members/getBackchecks',
    async () => {
        const response = await axios.get(`${PREFIX}backchecks`)
        return response.data
    }
)

export const changeBackcheck = createAsyncThunk(
    'members/changeBackcheck',
    async (id, payload) => {
        const response = await axios.patch(`${PREFIX}backchecks/${id}`, payload)
        return response.data
    }
)

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    members: [],
    selectedMember: {},
    appointmentHistory: [],
    trainingAnalytics: [],
    analyticsCategories: [],
    rechecks: [],
    singleRecheck: [],
    backchecks: [],
    anamnesis: [],
    error: null,
    status: null,
}

export const MembersSlice = createSlice({
    name: 'members',
    initialState,
    reducers: {},
    extraReducers: {
        // Get members list
        [getAllMembers.pending]: setLoading,
        [getAllMembers.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.members = action.payload
        },
        [getAllMembers.rejected]: setError,

        // Get single member
        [getMemberById.pending]: setLoading,
        [getMemberById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.selectedMember = action.payload
        },
        [getMemberById.rejected]: setError,

        // Search members
        [searchMembers.pending]: setLoading,
        [searchMembers.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.members = action.payload
            state.error = null
        },
        [searchMembers.rejected]: setError,

        // Create new member
        [createMember.pending]: setLoading,
        [createMember.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.locations.push(action.payload)
            state.selectedLocation = action.payload
            state.actionStatus = 'created'
        },
        [createMember.rejected]: setError,

        // Change member
        [changeMemberById.pending]: setLoading,
        [changeMemberById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.members.findIndex(
                (item) => item.id === parseInt(action.payload.id)
            )
            state.members[itemId] = action.payload
            state.selectedMember = action.payload
        },
        [changeMemberById.rejected]: setError,

        // Delete member
        [deleteMemberById.pending]: setLoading,
        [deleteMemberById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.members.findIndex(
                (item) => item.id === parseInt(action.meta.arg)
            )
            state.members.splice(itemId, 1)
        },
        [deleteMemberById.rejected]: setError,

        // Get appointment history
        [getAppointmentHistory.pending]: setLoading,
        [getAppointmentHistory.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.appointmentHistory = action.payload
        },
        [getAppointmentHistory.rejected]: setError,

        // Get anamnesis
        [getAnamnesis.pending]: setLoading,
        [getAnamnesis.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.anamnesis = action.payload
        },
        [getAnamnesis.rejected]: setError,

        // Get rechecks
        [getRechecks.pending]: setLoading,
        [getRechecks.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.rechecks = action.payload
        },
        [getRechecks.rejected]: setError,

        // Get single recheck
        [getSingleRecheck.pending]: setLoading,
        [getSingleRecheck.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.singleRecheck = action.payload
        },
        [getSingleRecheck.rejected]: setError,

        // Get single recheck
        [getTrainingAnalytics.pending]: setLoading,
        [getTrainingAnalytics.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            console.log(action.payload)
            state.trainingAnalytics = action.payload
            const categoriesFiltered = [
                ...new Set(action.payload.map((object) => object.category)),
            ]

            let dictionary = Object.assign(
                {},
                ...categoriesFiltered.map((x, index) => ({ [x]: false }))
            )

            state.analyticsCategories = dictionary
        },
        [getTrainingAnalytics.rejected]: setError,

        // Get backchecks
        [getBackchecks.pending]: setLoading,
        [getBackchecks.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.backchecks = action.payload
        },
        [getBackchecks.rejected]: setError,

        // Change single backcheck
        [changeBackcheck.pending]: setLoading,
        [changeBackcheck.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.backchecks = action.payload
        },
        [changeBackcheck.rejected]: setError,
    },
})

export default MembersSlice.reducer
