
import React from 'react'
import { Breadcrumb, SimpleCard } from 'app/components'
import { Box, styled } from '@mui/system'


const ProductsApp = () => {

    const Container = styled('div')(({ theme }) => ({
        margin: '30px',
        [theme.breakpoints.down('sm')]: {
            margin: '16px',
        },
        '& .breadcrumb': {
            marginBottom: '10px',
            [theme.breakpoints.down('sm')]: {
                marginBottom: '16px',
            },
        },
    }))

    return (
        <Container>
        <div className="breadcrumb">
            <Breadcrumb
                 routeSegments={[
                    { name: 'Products', path: '/products' },
                ]}
            />
        </div>
        <Box sx={{ py: '10px' }} />
                <SimpleCard title="List of products">
                </SimpleCard>
    </Container>
    )
}

export default ProductsApp
