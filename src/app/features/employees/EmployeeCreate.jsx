import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Breadcrumb } from 'app/components'
import { Container } from 'app/components/styled/Container'
import EmployeeProfile from './profile/EmployeeProfile'
import { getCountries } from 'app/redux/reducers/LibrariesSlice'
import { getCities } from 'app/redux/reducers/LibrariesSlice'
import { getCompetences } from 'app/redux/reducers/LibrariesSlice'
import { getLocations } from 'app/redux/reducers/LibrariesSlice'
import { setActiveForm } from './EmployeesSlice'

const EmployeeEdit = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getCountries())
        dispatch(getCities())
        dispatch(getCompetences())
        dispatch(getLocations())
        dispatch(setActiveForm('create'))
    }, [dispatch])

    return (
        <div>
            <Container>
                <div className="breadcrumb">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Employees', path: '/employees' },
                            {
                                name: 'Add new employee',
                                path: '/employees/create',
                            },
                        ]}
                    />
                </div>
                <EmployeeProfile />
            </Container>
        </div>
    )
}

export default EmployeeEdit
