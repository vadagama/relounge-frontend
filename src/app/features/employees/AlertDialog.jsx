import * as React from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'

export default function AlertDialog(props) {
    const { open, onClose, onDelete, value } = props

    return (
        <div>
            <Dialog open={open} onClose={onClose}>
                <DialogTitle>Delete employee</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Do you really want to delete employee?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" variant="text" onClick={onClose}>
                        Cancel
                    </Button>
                    <Button
                        color="error"
                        variant="text"
                        onClick={onDelete}
                        autoFocus
                    >
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}
