import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import IconButton from '@mui/material/IconButton'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import Tooltip from '@mui/material/Tooltip'
import Button from '@mui/material/Button'
import TablePagination from '@mui/material/TablePagination'
import { StyledTable } from '../../components/styled/StyledTable'
import Avatar from '@mui/material/Avatar'
import DeleteIcon from '@mui/icons-material/Delete'
import CreateIcon from '@mui/icons-material/Create'
import ErrorMessage from './ErrorMessage'
import AlertDialog from './AlertDialog'
import { deleteEmployeeById } from './EmployeesSlice'

const EmployeesTable = () => {
    const [rowsPerPage, setRowsPerPage] = React.useState(5)
    const [page, setPage] = React.useState(0)
    const [dialogIsOpen, setDialogIsOpen] = useState(false)
    const [currentItem, setItem] = useState()
    let navigate = useNavigate()
    const dispatch = useDispatch()

    const { employees, error } = useSelector((state) => state.employees)
    //if (isLoading) return <LoadingBackdrop />;
    if (error) return <ErrorMessage />

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value)
        setPage(0)
    }

    const handeDeleteClick = (event) => {
        event.preventDefault()
        console.log(event.currentTarget.value)
        setItem(event.currentTarget.value)
        openDialog()
    }

    const handeEditClick = (event) => {
        event.preventDefault()
        console.log(event.currentTarget.value)
        navigate(`edit/${event.currentTarget.value}`, { replace: true })
    }

    const openDialog = () => setDialogIsOpen(true)
    const closeDialog = () => setDialogIsOpen(false)

    const deleteEmployeeDialog = async () => {
        try {
            dispatch(deleteEmployeeById(currentItem))

            if (error) {
                return <ErrorMessage />
            } else {
                setDialogIsOpen(false)
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <>
            <AlertDialog
                open={dialogIsOpen}
                onDelete={deleteEmployeeDialog}
                onClose={closeDialog}
                value={currentItem}
            />
            <StyledTable>
                <TableHead>
                    <TableRow>
                        <TableCell style={{ width: 80 }}></TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Competences</TableCell>
                        <TableCell>Locations</TableCell>
                        <TableCell width={100}>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {employees
                        .slice(
                            page * rowsPerPage,
                            page * rowsPerPage + rowsPerPage
                        )
                        .map((employee, index) => (
                            <TableRow key={index}>
                                <TableCell align="left">
                                    <Avatar
                                        alt="Remy Sharp"
                                        src={employee.avatar}
                                    />
                                </TableCell>
                                <TableCell align="left">
                                    <Button
                                        value={employee.id}
                                        onClick={handeEditClick}
                                    >
                                        {employee.firstName} &nbsp;
                                        {employee.lastName}
                                    </Button>
                                </TableCell>
                                <TableCell align="left">
                                    {employee.selectedCompetences.map(
                                        (competence, index) => (
                                            <div key={index}>{competence}</div>
                                        )
                                    )}
                                </TableCell>
                                <TableCell align="left">
                                    {employee.selectedLocations.map(
                                        (location, index) => (
                                            <div key={index}>{location}</div>
                                        )
                                    )}
                                </TableCell>
                                <TableCell>
                                    <IconButton
                                        value={employee.id}
                                        onClick={handeEditClick}
                                    >
                                        <Tooltip title="Edit">
                                            <CreateIcon />
                                        </Tooltip>
                                    </IconButton>
                                    <IconButton
                                        value={employee.id}
                                        onClick={handeDeleteClick}
                                    >
                                        <Tooltip title="Delete">
                                            <DeleteIcon />
                                        </Tooltip>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </StyledTable>
            <TablePagination
                sx={{ px: 2 }}
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={employees.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page',
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </>
    )
}

export default EmployeesTable
