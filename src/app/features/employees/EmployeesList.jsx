import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import Button from '@mui/material/Button'
import { Breadcrumb, SimpleCard } from 'app/components'
import { Box } from '@mui/system'
import { Container } from '../../components/styled/Container'
import AddCircleIcon from '@mui/icons-material/AddCircle'
import { Link } from 'react-router-dom'
import Stack from '@mui/material/Stack'
import IconButton from '@mui/material/IconButton'
import FilterIcon from '@mui/icons-material/FilterList'
import EmployeesTable from './EmployeesTable'
import { EmployeesFilters } from 'app/components'
import {
    filterEmployeesByLocations,
    getAllEmployees,
    searchEmployees,
    setLocationsFilterValue,
} from './EmployeesSlice'
import { getLocations } from 'app/redux/reducers/LibrariesSlice'

const EmployeesList = () => {
    const dispatch = useDispatch()
    const [isVisible, setVisible] = React.useState(false)
    const { locations } = useSelector((state) => state.libraries)

    useEffect(() => {
        dispatch(getAllEmployees())
        dispatch(getLocations())
    }, [dispatch])

    const handleShowFilter = () => {
        setVisible(true)
    }

    return (
        <div>
            <Container>
                <div className="breadcrumb">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Employees', path: '/employees' },
                        ]}
                    />
                </div>
                <div>
                    <Link to="/employees/create">
                        <Button
                            variant="contained"
                            startIcon={<AddCircleIcon />}
                        >
                            Add an employee
                        </Button>
                    </Link>
                </div>
                <Box sx={{ py: '6px' }} />
                <SimpleCard title="Employees">
                    {isVisible ? (
                        <div>
                            <EmployeesFilters
                                setVisible={setVisible}
                                isVisible={isVisible}
                                locations={locations}
                                setLocationsFilterValue={(value) => {
                                    dispatch(setLocationsFilterValue(value))
                                }}
                                filterItemsByLocations={(value) => {
                                    dispatch(filterEmployeesByLocations(value))
                                }}
                                searchItems={(value) => {
                                    dispatch(searchEmployees(value))
                                }}
                                getAllItems={() => {
                                    dispatch(getAllEmployees())
                                }}
                            />
                        </div>
                    ) : (
                        <Stack direction="row" justifyContent="flex-end">
                            <IconButton
                                onClick={handleShowFilter}
                                edge="end"
                                sx={{
                                    width: 50,
                                    height: 50,
                                    position: 'absolute',
                                    marginRight: '5px',
                                    marginTop: '-50px',
                                }}
                            >
                                <FilterIcon />
                            </IconButton>
                        </Stack>
                    )}
                    <Box width="100%" overflow="auto">
                        <EmployeesTable />
                    </Box>
                </SimpleCard>
            </Container>
        </div>
    )
}

export default EmployeesList
