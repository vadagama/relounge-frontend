import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { ValidatorForm } from 'react-material-ui-form-validator'
import { Span } from 'app/components/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import Autocomplete from '@mui/material/Autocomplete'
import Stack from '@mui/material/Stack'
import OutlinedInput from '@mui/material/OutlinedInput'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import ListItemText from '@mui/material/ListItemText'
import Select from '@mui/material/Select'
import Checkbox from '@mui/material/Checkbox'
import { ImageButton } from 'app/components/styled/ImageButton'
import { ImageSrc } from 'app/components/styled/ImageSrc'
import { Image } from 'app/components/styled/Image'
import { Input } from 'app/components/styled/Input'
import { ImageBackdrop } from 'app/components/styled/ImageBackdrop'
import { Field } from 'app/components/styled/Field'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { SimpleCard } from 'app/components'
import { NavLink } from 'react-router-dom'
import ErrorMessage from '../ErrorMessage'
import { createEmployee } from '../EmployeesSlice'
import { changeEmployeeById } from '../EmployeesSlice'

// Multiselect control
const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
}

const EmployeeProfile = (props) => {
    const dispatch = useDispatch()
    const [state, setState] = useState()
    const now = Date.now()
    let navigate = useNavigate()

    const { selectedEmployee, activeForm, error } = useSelector(
        (state) => state.employees
    )

    const { status, countries, competences, locations, cities } = useSelector(
        (state) => state.libraries
    )

    console.log(state)

    useEffect(() => {
        activeForm === 'create' &&
            setState({
                ...state,
                countries: countries,
                cities: cities,
                locations: locations,
                competences: competences,
                selectedLocations: [],
                selectedCompetences: [],
                birthday: new Date(now),
                firstName: '',
                lastName: '',
                phone: '',
                email: '',
                city: '',
                country: '',
                street: '',
                building: '',
                postalCode: '',
            })

        activeForm === 'edit' &&
            setState({
                ...state,
                countries: countries,
                cities: cities,
                locations: locations,
                competences: competences,
                ...selectedEmployee,
            })
    }, [
        state,
        now,
        countries,
        cities,
        locations,
        competences,
        selectedEmployee,
        activeForm,
    ])

    const onCountryChange = (event, value) => {
        setState({
            ...state,
            country: value,
        })
    }

    const onCityChange = (event, value) => {
        setState({
            ...state,
            city: value,
        })
    }

    const handleChange = (event) => {
        event.persist()
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    const handleLocationsSelect = (event) => {
        const {
            target: { value },
        } = event
        setState({
            ...state,
            selectedLocations: value,
        })
    }

    const handleCompetencesSelect = (event) => {
        const {
            target: { value },
        } = event

        setState({
            ...state,
            selectedCompetences: value,
        })
    }

    const handleSubmit = async (event) => {
        const updatedPerson = {
            avatar: state.avatar,
            firstName: state.firstName,
            lastName: state.lastName,
            birthday: state.birthday,
            selectedLocations: state.selectedLocations,
            selectedCompetences: state.selectedCompetences,
            city: state.city,
            country: state.country,
            email: state.email,
            phone: state.phone,
            postalCode: state.postalCode,
            street: state.street,
            building: state.building,
        }

        let updatedPersonForEdit = {
            ...updatedPerson,
            id: selectedEmployee.id,
        }
        event.preventDefault()
        try {
            activeForm === 'create' && dispatch(createEmployee(updatedPerson))
            activeForm === 'edit' &&
                dispatch(changeEmployeeById(updatedPersonForEdit))

            if (error) {
                return <ErrorMessage />
            } else {
                navigate('/employees')
            }
        } catch (err) {
            console.log('api error')
        }
    }

    return (
        <div>
            {status === 'resolved' && state && (
                <SimpleCard
                    title={
                        activeForm === 'create'
                            ? 'Add new employee'
                            : 'Edit employee'
                    }
                >
                    <ValidatorForm onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item lg={2} md={3} sm={12} xs={12}>
                                <Box
                                    sx={{
                                        display: 'flex',
                                        flexWrap: 'wrap',
                                        minWidth: 300,
                                        width: '100%',
                                    }}
                                >
                                    <label htmlFor="contained-button-file">
                                        <Input
                                            accept="image/*"
                                            id="contained-button-file"
                                            multiple
                                            type="file"
                                        />
                                        <ImageButton
                                            variant="contained"
                                            component="span"
                                            focusRipple
                                            style={{
                                                width: '100px',
                                                height: '100px',
                                            }}
                                        >
                                            <ImageSrc
                                                style={{
                                                    backgroundImage: `url(${state.avatar})`,
                                                }}
                                            />
                                            <ImageBackdrop className="MuiImageBackdrop-root" />
                                            <Image>
                                                <Typography
                                                    component="span"
                                                    variant="subtitle1"
                                                    color="inherit"
                                                    sx={{
                                                        position: 'relative',
                                                        p: 1,
                                                        pt: 1,
                                                        pb: (theme) =>
                                                            `calc(${theme.spacing(
                                                                1
                                                            )} + 3px)`,
                                                    }}
                                                >
                                                    Upload
                                                </Typography>
                                            </Image>
                                        </ImageButton>
                                    </label>
                                </Box>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Field
                                    label="First Name"
                                    name="firstName"
                                    value={state.firstName || ''}
                                    type="text"
                                    onChange={handleChange}
                                    validators={[
                                        'required',
                                        'minStringLength: 3',
                                        'maxStringLength: 30',
                                    ]}
                                    errorMessages={['this field is required']}
                                />
                                <Field
                                    label="LastName"
                                    name="lastName"
                                    value={state.lastName || ''}
                                    type="text"
                                    onChange={handleChange}
                                    validators={[
                                        'required',
                                        'minStringLength: 3',
                                        'maxStringLength: 30',
                                    ]}
                                    errorMessages={['this field is required']}
                                />
                                <LocalizationProvider
                                    dateAdapter={AdapterDateFns}
                                >
                                    <DatePicker
                                        label="Birthday"
                                        inputFormat="MM/dd/yyyy"
                                        value={state.birthday}
                                        onChange={(newValue) => {
                                            setState({
                                                ...state,
                                                birthday: newValue,
                                            })
                                        }}
                                        renderInput={(params) => (
                                            <Field {...params} />
                                        )}
                                    />
                                </LocalizationProvider>
                                <Field
                                    label="Email"
                                    onChange={handleChange}
                                    type="email"
                                    name="email"
                                    value={state.email || ''}
                                    validators={['required', 'isEmail']}
                                    errorMessages={[
                                        'this field is required',
                                        'email is not valid',
                                    ]}
                                />
                            </Grid>

                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Field
                                    label="Phone"
                                    onChange={handleChange}
                                    type="text"
                                    name="phone"
                                    value={state.phone || ''}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                                <FormControl
                                    sx={{
                                        width: '100%',
                                        paddingBottom: '16px',
                                    }}
                                >
                                    <InputLabel id="demo-multiple-checkbox-label">
                                        Locations
                                    </InputLabel>
                                    <Select
                                        multiple
                                        value={state.selectedLocations}
                                        onChange={handleLocationsSelect}
                                        input={
                                            <OutlinedInput label="Locations" />
                                        }
                                        renderValue={(selected) =>
                                            selected.join(', ')
                                        }
                                        MenuProps={MenuProps}
                                    >
                                        {state.locations.map(
                                            (location, index) => (
                                                <MenuItem
                                                    key={location}
                                                    value={location}
                                                >
                                                    <Checkbox
                                                        checked={
                                                            state.selectedLocations.indexOf(
                                                                location
                                                            ) > -1
                                                        }
                                                    />
                                                    <ListItemText
                                                        primary={location}
                                                    />
                                                </MenuItem>
                                            )
                                        )}
                                    </Select>
                                </FormControl>
                                <FormControl
                                    sx={{
                                        width: '100%',
                                        paddingBottom: '16px',
                                    }}
                                >
                                    <InputLabel id="demo-multiple-checkbox-label">
                                        Competences
                                    </InputLabel>
                                    <Select
                                        multiple
                                        value={state.selectedCompetences}
                                        onChange={handleCompetencesSelect}
                                        input={
                                            <OutlinedInput label="Competences" />
                                        }
                                        renderValue={(selected) =>
                                            selected.join(', ')
                                        }
                                        MenuProps={MenuProps}
                                    >
                                        {state.competences.map(
                                            (competence, index) => (
                                                <MenuItem
                                                    key={competence}
                                                    value={competence}
                                                >
                                                    <Checkbox
                                                        checked={
                                                            state.selectedCompetences.indexOf(
                                                                competence
                                                            ) > -1
                                                        }
                                                    />
                                                    <ListItemText
                                                        primary={competence}
                                                    />
                                                </MenuItem>
                                            )
                                        )}
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Divider>Address</Divider>
                        <Box sx={{ py: '10px' }} />

                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Autocomplete
                                    disablePortal
                                    id="combo-box-demo"
                                    name="country"
                                    onChange={onCountryChange}
                                    value={state.country}
                                    options={state.countries}
                                    sx={{ width: '100%' }}
                                    renderInput={(params) => (
                                        <Field
                                            {...params}
                                            name="country"
                                            label="Country"
                                        />
                                    )}
                                />
                                <Field
                                    label="Street"
                                    onChange={handleChange}
                                    type="text"
                                    name="street"
                                    value={state.street || ''}
                                />
                                <Field
                                    label="Postal Code"
                                    onChange={handleChange}
                                    type="text"
                                    name="postalCode"
                                    value={state.postalCode || ''}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Autocomplete
                                    disablePortal
                                    id="combo-box-demo"
                                    options={state.cities}
                                    onChange={onCityChange}
                                    value={state.city}
                                    sx={{ width: '100%' }}
                                    renderInput={(params) => (
                                        <Field
                                            {...params}
                                            name="city"
                                            label="City"
                                        />
                                    )}
                                />
                                <Field
                                    label="Building"
                                    onChange={handleChange}
                                    type="text"
                                    name="building"
                                    value={state.building || ''}
                                />
                            </Grid>
                        </Grid>
                        <Box sx={{ py: '6px' }} />
                        <Stack direction="row" spacing={2}>
                            <Button
                                color="primary"
                                variant="contained"
                                type="submit"
                            >
                                <Span>Save changes</Span>
                            </Button>
                            <NavLink to="/employees">
                                <Button color="error" variant="contained">
                                    <Span>Cancel</Span>
                                </Button>
                            </NavLink>
                        </Stack>
                    </ValidatorForm>
                </SimpleCard>
            )}
        </div>
    )
}

export default EmployeeProfile
