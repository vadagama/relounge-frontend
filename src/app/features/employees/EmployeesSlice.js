import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getAllEmployees = createAsyncThunk(
    'employees/getAllEmployees',
    async () => {
        const response = await axios.get(`${PREFIX}employees`)
        return response.data
    }
)

export const searchEmployees = createAsyncThunk(
    'employees/searchEmployees',
    async (payload) => {
        const response = await axios.get(`${PREFIX}employees?q=${payload}`)
        return response.data
    }
)

export const filterEmployeesByLocations = createAsyncThunk(
    'employees/filterEmployeesByLocations',
    async (payload) => {
        let results_array = []
        for (let item in payload) {
            const response = await axios.get(
                `${PREFIX}employees?selectedLocations=${payload[item]}`
            )
            results_array.push(...response.data)
        }
        return results_array
    }
)

export const getEmployeeById = createAsyncThunk(
    'employees/getEmployeeById',
    async (id) => {
        const response = await axios.get(`${PREFIX}employees/${id}`)
        return response.data
    }
)

export const createEmployee = createAsyncThunk(
    'employees/createEmployee',
    async (payload) => {
        const response = await axios.post(`${PREFIX}employees`, payload)
        return response.data
    }
)

export const changeEmployeeById = createAsyncThunk(
    'employees/changeEmployeeById',
    async (payload) => {
        const response = await axios.put(
            `${PREFIX}employees/${payload.id}`,
            payload
        )
        return response.data
    }
)

export const deleteEmployeeById = createAsyncThunk(
    'employees/deleteEmployeeById',
    async (id) => {
        const response = await axios.delete(`${PREFIX}employees/${id}`)
        return response.data
    }
)

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    employees: [],
    isLoading: false,
    selectedEmployee: {},
    employeeLocations: [],
    employeeCompetences: [],
    locationsFilterValue: [],
    activeForm: null,
    status: null,
    error: null,
}

export const EmployeesSlice = createSlice({
    name: 'employees',
    initialState,
    reducers: {
        setActiveForm(state, action) {
            state.activeForm = action.payload
        },
        setLocationsFilterValue(state, action) {
            state.CompetencesFilterValue = action.payload
        },
    },
    extraReducers: {
        // Get employees list
        [getAllEmployees.pending]: setLoading,
        [getAllEmployees.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.employees = action.payload
        },
        [getAllEmployees.rejected]: setError,

        // Search employees
        [searchEmployees.pending]: setLoading,
        [searchEmployees.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            // state.employees = state.employees.filter((row) => {
            //     if (!action.payload) return row

            //     return row.subject
            //         .toLowerCase()
            //         .includes(action.payload.toLowerCase())
            // })
            state.employees = action.payload
        },
        [searchEmployees.rejected]: setError,

        // Filter employees by competences
        [filterEmployeesByLocations.pending]: setLoading,
        [filterEmployeesByLocations.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.employees = action.payload
            state.error = null
        },
        [filterEmployeesByLocations.rejected]: setError,

        // Get single employee
        [getEmployeeById.pending]: setLoading,
        [getEmployeeById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.selectedEmployee = action.payload
            state.employeeLocations = action.payload.locations
            state.employeeCompetences = action.payload.competences
        },
        [getEmployeeById.rejected]: setError,

        // Create employee
        [createEmployee.pending]: setLoading,
        [createEmployee.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.employees.push(action.payload)
            state.selectedEmployee = action.payload
        },
        [createEmployee.rejected]: setError,

        // Change employee
        [changeEmployeeById.pending]: setLoading,
        [changeEmployeeById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.employees.findIndex(
                (item) => item.id === parseInt(action.payload.id)
            )
            state.employees[itemId] = action.payload
            state.selectedEmployee = action.payload
        },
        [changeEmployeeById.rejected]: setError,

        // Delete employee
        [deleteEmployeeById.pending]: setLoading,
        [deleteEmployeeById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.employees.findIndex(
                (item) => item.id === parseInt(action.meta.arg)
            )
            state.employees.splice(itemId, 1)
        },
        [deleteEmployeeById.rejected]: setError,
    },
})

export const { setActiveForm, setLocationsFilterValue } = EmployeesSlice.actions

export default EmployeesSlice.reducer
