import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { Container } from '../../components/styled/Container'
import { Breadcrumb } from 'app/components'
import { getEmployeeById } from './EmployeesSlice'
import { getCountries } from 'app/redux/reducers/LibrariesSlice'
import { getCities } from 'app/redux/reducers/LibrariesSlice'
import { getCompetences } from 'app/redux/reducers/LibrariesSlice'
import { getLocations } from 'app/redux/reducers/LibrariesSlice'
import EmployeeProfile from './profile/EmployeeProfile'
import { setActiveForm } from './EmployeesSlice'

const EmployeeEdit = (props) => {
    const dispatch = useDispatch()
    const params = useParams()

    useEffect(() => {
        dispatch(getEmployeeById(params.id))
    }, [dispatch, params.id])

    useEffect(() => {
        dispatch(getCountries())
        dispatch(getCities())
        dispatch(getCompetences())
        dispatch(getLocations())
        dispatch(setActiveForm('edit'))
    }, [dispatch])

    return (
        <div>
            <Container>
                <div className="breadcrumb">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Employees', path: '/employees' },
                            {
                                name: 'Edit employee',
                                path: `/employees/${params.id}`,
                            },
                        ]}
                    />
                </div>
                <EmployeeProfile />
            </Container>
        </div>
    )
}

export default EmployeeEdit
