import React, { lazy } from 'react'
import Loadable from 'app/components/Loadable/Loadable'
import { authRoles } from '../../auth/authRoles'

const EmployeesList = Loadable(lazy(() => import('./EmployeesList')))
const EmployeeCreate = Loadable(lazy(() => import('./EmployeeCreate')))
const EmployeeEdit = Loadable(lazy(() => import('./EmployeeEdit')))

const EmployeesRoutes = [
    {
        path: '/employees',
        element: <EmployeesList />,
        auth: authRoles.admin,
    },
    {
        path: '/employees/create',
        element: <EmployeeCreate />,
        auth: authRoles.admin,
    },
    {
        path: '/employees/edit/:id',
        element: <EmployeeEdit />,
        auth: authRoles.admin,
    },
]

export default EmployeesRoutes
