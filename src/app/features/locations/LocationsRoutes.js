import React, { lazy } from 'react'
import Loadable from 'app/components/Loadable/Loadable'
import { authRoles } from '../../auth/authRoles'

const LocationsApp = Loadable(lazy(() => import('./LocationsApp')))
const LocationsEdit = Loadable(lazy(() => import('./LocationsEdit')))
const LocationCreate = Loadable(lazy(() => import('./LocationCreate')))

const LocationsRoutes = [
    {
        path: '/locations',
        element: <LocationsApp />,
        auth: authRoles.admin,
    },
    {
        path: '/locations/:id',
        element: <LocationsEdit />,
        auth: authRoles.admin,
    },
    {
        path: '/locations/create',
        element: <LocationCreate />,
        auth: authRoles.admin,
    },
]

export default LocationsRoutes
