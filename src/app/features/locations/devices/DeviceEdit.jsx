import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { ValidatorForm } from 'react-material-ui-form-validator'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Chip from '@mui/material/Chip'
import Divider from '@mui/material/Divider'
import { styled } from '@mui/system'
import AddIcon from '@mui/icons-material/Add'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import TextField from '@mui/material/TextField'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { changeDevices } from './DevicesSlice'
import { deleteDeviceById } from './DevicesSlice'

const Field = styled(TextField)(() => ({
    width: '100%',
    marginBottom: '16px',
}))

const DeviceEdit = ({ selectedIndex }) => {
    const [state, setState] = useState()

    const { deviceList } = useSelector((state) => state.devices)
    const dispatch = useDispatch()

    const handleSubmit = (event) => {
        dispatch(changeDevices(state))
    }

    const handleDeleteDevice = (event) => {
        dispatch(deleteDeviceById(deviceList[selectedIndex].id))
    }

    useEffect(() => {
        setState(deviceList[selectedIndex])
    }, [selectedIndex, deviceList])

    const handleChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    const handleChangelastCheckDate = (value) => {
        setState({
            ...state,
            lastCheckDate: value,
        })
    }

    const handleChangewarrantyTill = (value) => {
        setState({
            ...state,
            warrantyTill: value,
        })
    }

    const handleAddInterval = () => {
        const sectionToUpdate = [...state.workingHours]
        sectionToUpdate.push({
            weekday: weekday,
            periods: [
                {
                    start: startInterval,
                    finish: stopInterval,
                },
            ],
        })

        setState((prevState) => ({
            ...prevState,
            workingHours: sectionToUpdate,
        }))
        setOpenIntervalDialog(false)
    }

    const handleDeleteInterval = (hoursIndex, periodsIndex) => {
        const periodsToUpdate = [...state.workingHours[hoursIndex].periods]
        periodsToUpdate.splice(periodsIndex, 1)

        if (periodsToUpdate.length === 0) {
            const hoursToUpdate = [...state.workingHours]
            hoursToUpdate.splice(hoursIndex, 1)
            setState((prevState) => ({
                ...prevState,
                workingHours: hoursToUpdate,
            }))
        } else {
            setState((prevState) => ({
                ...prevState,
                workingHours: prevState.workingHours.map((item, index) =>
                    index === hoursIndex
                        ? { ...item, periods: [...periodsToUpdate] }
                        : item
                ),
            }))
        }
    }

    const handleAddException = () => {
        const sectionToUpdate = [...state.exceptions]
        sectionToUpdate.push({
            weekday: exceptionDate.toDateString(),
            periods: [
                {
                    start: startInterval,
                    finish: stopInterval,
                },
            ],
        })

        setState((prevState) => ({
            ...prevState,
            exceptions: sectionToUpdate,
        }))
        setOpenExceptionDialog(false)
    }

    const handleDeleteException = (hoursIndex, periodsIndex) => {
        const periodsToUpdate = [...state.exceptions[hoursIndex].periods]
        periodsToUpdate.splice(periodsIndex, 1)

        if (periodsToUpdate.length === 0) {
            const hoursToUpdate = [...state.exceptions]
            hoursToUpdate.splice(hoursIndex, 1)
            setState((prevState) => ({
                ...prevState,
                exceptions: hoursToUpdate,
            }))
        } else {
            setState((prevState) => ({
                ...prevState,
                exceptions: prevState.exceptions.map((item, index) =>
                    index === hoursIndex
                        ? { ...item, periods: [...periodsToUpdate] }
                        : item
                ),
            }))
        }
    }

    const [openIntervalDialog, setOpenIntervalDialog] = React.useState(false)
    const [openExceptionDialog, setOpenExceptionDialog] = React.useState(false)
    const [weekday, setWeekday] = React.useState('')
    const [startInterval, setStart] = React.useState('')
    const [stopInterval, setStop] = React.useState('')
    const now = Date.now()
    const [exceptionDate, setExceptionDate] = useState(new Date(now))
    const handleDateChange = (newValue) => {
        setExceptionDate(newValue)
    }

    const handleWeekday = (event) => {
        setWeekday(event.target.value)
    }

    const handleStart = (event) => {
        setStart(event.target.value)
    }

    const handleStop = (event) => {
        setStop(event.target.value)
    }

    const handleClickOpen = (type) => {
        type === 'interval'
            ? setOpenIntervalDialog(true)
            : setOpenExceptionDialog(true)
    }

    const handleClose = (type) => {
        type === 'interval'
            ? setOpenIntervalDialog(false)
            : setOpenExceptionDialog(false)
    }

    const weekdays = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
    ]

    let timeIntervals = []

    for (let i = 8; i <= 19; i++) {
        timeIntervals.push(`${i}:00`)
        timeIntervals.push(`${i}:30`)
    }

    return (
        <div>
            <Dialog
                open={openIntervalDialog}
                onClose={() => {
                    handleClose('interval')
                }}
            >
                <DialogTitle>{'Add working interval'}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Choose weekday and propriate interval:
                    </DialogContentText>
                    <Box sx={{ py: '6px' }} />
                    <FormControl sx={{ m: 1, minWidth: 140 }}>
                        <InputLabel>Weekday</InputLabel>
                        <Select
                            value={weekday}
                            label="Weekday"
                            onChange={handleWeekday}
                        >
                            {weekdays.map((item, index) => (
                                <MenuItem key={index} value={item}>
                                    {item}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    <FormControl sx={{ m: 1, minWidth: 140 }}>
                        <InputLabel>Start Interval</InputLabel>
                        <Select
                            value={startInterval}
                            label="Start Interval"
                            onChange={handleStart}
                        >
                            {timeIntervals.map((item, index) => (
                                <MenuItem key={index} value={item}>
                                    {item}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    <FormControl sx={{ m: 1, minWidth: 140 }}>
                        <InputLabel>Stop Interval</InputLabel>
                        <Select
                            value={stopInterval}
                            label="Stop Interval"
                            onChange={handleStop}
                        >
                            {timeIntervals.map((item, index) => (
                                <MenuItem key={index} value={item}>
                                    {item}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        color="error"
                        onClick={() => {
                            handleClose('interval')
                        }}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleAddInterval}
                        autoFocus
                    >
                        Add interval
                    </Button>
                </DialogActions>
            </Dialog>

            <Dialog
                open={openExceptionDialog}
                onClose={() => {
                    handleClose('exception')
                }}
            >
                <DialogTitle>{'Add working interval'}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Choose weekday and propriate interval:
                    </DialogContentText>
                    <Box sx={{ py: '6px' }} />

                    <FormControl sx={{ m: 1, minWidth: 140 }}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                                label="Date"
                                inputFormat="MM/dd/yyyy"
                                value={exceptionDate}
                                onChange={handleDateChange}
                                renderInput={(params) => <Field {...params} />}
                            />
                        </LocalizationProvider>
                    </FormControl>

                    <FormControl sx={{ m: 1, minWidth: 140 }}>
                        <InputLabel>Start Interval</InputLabel>
                        <Select
                            value={startInterval}
                            label="Start Interval"
                            onChange={handleStart}
                        >
                            {timeIntervals.map((item, index) => (
                                <MenuItem key={index} value={item}>
                                    {item}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    <FormControl sx={{ m: 1, minWidth: 140 }}>
                        <InputLabel>Stop Interval</InputLabel>
                        <Select
                            value={stopInterval}
                            label="Stop Interval"
                            onChange={handleStop}
                        >
                            {timeIntervals.map((item, index) => (
                                <MenuItem key={index} value={item}>
                                    {item}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        color="error"
                        onClick={() => {
                            handleClose('exception')
                        }}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleAddException}
                        autoFocus
                    >
                        Add interval
                    </Button>
                </DialogActions>
            </Dialog>

            {state && (
                <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
                    <Grid container spacing={2}>
                        <Grid item lg={8} md={8} sm={12} xs={12}>
                            <h2>{state.name || ''}</h2>
                        </Grid>

                        <Grid item lg={8} md={8} sm={12} xs={12}>
                            <Field
                                name="name"
                                label="Name"
                                type="text"
                                onChange={handleChange}
                                id="name"
                                value={state.name || ''}
                                validators={[
                                    'required',
                                    'minStringLength: 4',
                                    'maxStringLength: 50',
                                ]}
                                errormessages={['this field is required']}
                            />

                            <Field
                                name="uniqueNumber"
                                label="Unique Number"
                                type="text"
                                id="uniqueNumber"
                                onChange={handleChange}
                                value={state.uniqueNumber || ''}
                                validators={[
                                    'required',
                                    'minStringLength: 4',
                                    'maxStringLength: 15',
                                ]}
                                errormessages={['this field is required']}
                            />
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                <DatePicker
                                    label="Date"
                                    inputFormat="MM/dd/yyyy"
                                    value={state.lastCheckDate}
                                    onChange={handleChangelastCheckDate}
                                    renderInput={(params) => (
                                        <Field {...params} />
                                    )}
                                />
                            </LocalizationProvider>
                        </Grid>
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                <DatePicker
                                    label="Date"
                                    inputFormat="MM/dd/yyyy"
                                    value={state.warrantyTill}
                                    onChange={handleChangewarrantyTill}
                                    renderInput={(params) => (
                                        <Field {...params} />
                                    )}
                                />
                            </LocalizationProvider>
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        lg={8}
                        md={8}
                        sm={12}
                        xs={12}
                        sx={{ margin: '0px 6px 12px 0px ' }}
                    >
                        <Divider />
                    </Grid>

                    <h4>Working hours</h4>

                    {state.workingHours.length > 0 &&
                        state.workingHours.map((hoursItem, hoursIndex) => (
                            <div key={hoursIndex}>
                                <Grid
                                    container
                                    spacing={2}
                                    sx={{ alignItems: 'center' }}
                                >
                                    <Grid item lg={2} md={2} sm={12} xs={12}>
                                        <div>
                                            {hoursItem.periods.length > 0 &&
                                                hoursItem.weekday}
                                        </div>
                                    </Grid>

                                    <Grid
                                        key={hoursIndex}
                                        item
                                        lg={2}
                                        md={3}
                                        sm={12}
                                        xs={12}
                                    >
                                        <Stack
                                            alignItems="center"
                                            direction="row"
                                            spacing={1}
                                            sx={{ minWidth: '200px' }}
                                        >
                                            {hoursItem.periods.length > 0 &&
                                                hoursItem.periods.map(
                                                    (
                                                        periodsItem,
                                                        periodsIndex
                                                    ) => (
                                                        <Chip
                                                            key={periodsIndex}
                                                            sx={{
                                                                minWidth:
                                                                    '130px',
                                                            }}
                                                            label={`${periodsItem.start} - ${periodsItem.finish}`}
                                                            variant="outlined"
                                                            onDelete={() =>
                                                                handleDeleteInterval(
                                                                    hoursIndex,
                                                                    periodsIndex
                                                                )
                                                            }
                                                        />
                                                    )
                                                )}
                                        </Stack>
                                    </Grid>
                                </Grid>
                                <Box sx={{ py: '6px' }} />
                            </div>
                        ))}

                    <Button
                        onClick={() => {
                            handleClickOpen('interval')
                        }}
                        startIcon={<AddIcon />}
                    >
                        Add working interval
                    </Button>
                    <Grid
                        item
                        lg={8}
                        md={8}
                        sm={12}
                        xs={12}
                        sx={{ margin: '12px 0px' }}
                    >
                        <Divider />
                    </Grid>

                    <h4>Exceptions (non working)</h4>

                    {state.exceptions.length > 0 &&
                        state.exceptions.map((hoursItem, hoursIndex) => (
                            <div key={hoursIndex}>
                                <Grid
                                    container
                                    spacing={2}
                                    sx={{ alignItems: 'center' }}
                                >
                                    <Grid item lg={2} md={2} sm={12} xs={12}>
                                        <div>
                                            {hoursItem.periods.length > 0 &&
                                                hoursItem.weekday}
                                        </div>
                                    </Grid>

                                    <Grid
                                        key={hoursIndex}
                                        item
                                        lg={2}
                                        md={3}
                                        sm={12}
                                        xs={12}
                                    >
                                        <Stack
                                            alignItems="center"
                                            direction="row"
                                            spacing={1}
                                            sx={{ minWidth: '200px' }}
                                        >
                                            {hoursItem.periods.length > 0 &&
                                                hoursItem.periods.map(
                                                    (
                                                        periodsItem,
                                                        periodsIndex
                                                    ) => (
                                                        <Chip
                                                            key={periodsIndex}
                                                            sx={{
                                                                minWidth:
                                                                    '130px',
                                                            }}
                                                            label={`${periodsItem.start} - ${periodsItem.finish}`}
                                                            variant="outlined"
                                                            onDelete={() =>
                                                                handleDeleteException(
                                                                    hoursIndex,
                                                                    periodsIndex
                                                                )
                                                            }
                                                        />
                                                    )
                                                )}
                                        </Stack>
                                    </Grid>
                                </Grid>
                                <Box sx={{ py: '6px' }} />
                            </div>
                        ))}

                    <Button
                        onClick={() => {
                            handleClickOpen('exception')
                        }}
                        startIcon={<AddIcon />}
                    >
                        Add exception
                    </Button>

                    <Box sx={{ py: '16px' }} />
                    <Stack spacing={2} direction="row">
                        <Button
                            color="primary"
                            variant="contained"
                            type="submit"
                        >
                            Save Changes
                        </Button>
                        <Button
                            color="error"
                            variant="contained"
                            onClick={handleDeleteDevice}
                        >
                            Delete Device
                        </Button>
                    </Stack>
                </ValidatorForm>
            )}
        </div>
    )
}

export default DeviceEdit
