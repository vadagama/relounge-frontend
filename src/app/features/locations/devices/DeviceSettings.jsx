import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import { SimpleCard } from 'app/components'
import DeviceEdit from './DeviceEdit'
import { getDevices } from './DevicesSlice'
import { createDevice } from './DevicesSlice'

const DeviceSettings = ({ setActiveStep, actionType }) => {
    const dispatch = useDispatch()
    const now = Date.now()
    const emptyDevice = {
        id: Math.floor(Math.random() * 1000000),
        name: 'New Device',
        uniqueNumber: '98622345',
        lastCheckDate: new Date(now).toDateString(),
        warrantyTill: new Date(now).toDateString(),
        workingHours: [],
        exceptions: [],
    }
    const [selectedIndex, setSelectedIndex] = React.useState(0)
    const { deviceList } = useSelector((state) => state.devices)

    useEffect(() => {
        dispatch(getDevices())
    }, [dispatch])

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index)
    }

    const addNewDevice = () => {
        dispatch(createDevice(emptyDevice))
    }

    return (
        <div>
            <SimpleCard title="Device Settings">
                <Grid container spacing={3}>
                    <Grid item lg={3} md={6} xs={12}>
                        <ListItem disablePadding>
                            <h4>Devices</h4>
                        </ListItem>
                        <List>
                            {deviceList.map((item, index) => (
                                <ListItem disablePadding key={item.id}>
                                    <ListItemButton
                                        selected={selectedIndex === index}
                                        onClick={(event) =>
                                            handleListItemClick(event, index)
                                        }
                                    >
                                        <ListItemText primary={item.name} />
                                    </ListItemButton>
                                </ListItem>
                            ))}

                            <Button
                                variant="contained"
                                sx={{ mt: '10px' }}
                                onClick={addNewDevice}
                            >
                                Add device
                            </Button>
                        </List>
                    </Grid>
                    <Grid lg={9} md={6} item xs={6}>
                        <DeviceEdit
                            selectedIndex={selectedIndex}
                            setActiveStep={setActiveStep}
                            actionType={actionType}
                        />
                    </Grid>
                </Grid>
            </SimpleCard>
        </div>
    )
}

export default DeviceSettings
