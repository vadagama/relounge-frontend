import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getDevices = createAsyncThunk('devices/getDevices', async () => {
    const response = await axios.get(`${PREFIX}devices`)
    if (!response) {
        throw new Error("Server Error! Can't get devices.")
    }
    return response.data
})

export const createDevice = createAsyncThunk(
    'devices/createDevice',
    async (payload) => {
        const response = await axios.post(`${PREFIX}devices`, payload)
        if (!response) {
            throw new Error(`Server Error! Can't create device.`)
        }
        return response.data
    }
)

export const changeDevices = createAsyncThunk(
    'devices/changeDevices',
    async (payload) => {
        const response = await axios.put(
            `${PREFIX}devices/${payload.id}`,
            payload
        )
        if (!response) {
            throw new Error(`Server Error! Can't edit device.`)
        }
        return response.data
    }
)

export const deleteDeviceById = createAsyncThunk(
    'devices/deleteDeviceById',
    async (id) => {
        const response = await axios.delete(`${PREFIX}devices/${id}`)
        if (!response) {
            throw new Error(`Server Error! Can't delete device.`)
        }
        return response.data
    }
)

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    deviceList: [],
    deviceListBuffer: [],
    selectedItem: 0,
    status: null,
    error: null,
}

const DevicesSlice = createSlice({
    name: 'devices',
    initialState,
    reducers: {
        setSelectedItem: (state, action) => {
            state.selectedItem = action.payload
        },
    },
    extraReducers: {
        // Get device
        [getDevices.pending]: setLoading,
        [getDevices.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.deviceList = action.payload
        },
        [getDevices.rejected]: setError,

        // Create new device
        [createDevice.pending]: setLoading,
        [createDevice.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.deviceList.push(action.payload)
        },
        [createDevice.rejected]: setError,

        // Change device
        [changeDevices.pending]: setLoading,
        [changeDevices.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.deviceList.findIndex(
                (item) => item.id === parseInt(action.payload.id)
            )
            state.deviceList[itemId] = action.payload
        },
        [changeDevices.rejected]: setError,

        // Delete device
        [deleteDeviceById.pending]: setLoading,
        [deleteDeviceById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.deviceList.findIndex(
                (item) => item.id === parseInt(action.meta.arg)
            )
            state.deviceList.splice(itemId, 1)
        },
        [deleteDeviceById.rejected]: setError,
    },
})

export const { setSelectedItem } = DevicesSlice.actions

export default DevicesSlice.reducer
