import React from 'react'
import { ValidatorForm } from 'react-material-ui-form-validator'
import { useSelector } from 'react-redux'
import { SimpleCard } from 'app/components'
import { Link } from 'react-router-dom'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'

export const LocationReview = ({ actionType }) => {
    const { deviceList } = useSelector((state) => state.devices)
    const { selectedLocation } = useSelector((state) => state.locations)

    const handleSubmit = (event) => {
        console.log(deviceList)
    }

    return (
        <div>
            <SimpleCard title="Review new location">
                <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
                    <Box sx={{ py: '6px' }} />

                    <Box sx={{ width: '100%', maxWidth: 500 }}>
                        <Typography variant="h6" gutterBottom component="div">
                            {selectedLocation.name}
                        </Typography>
                        <Typography variant="body1" gutterBottom>
                            <strong>Description:</strong>{' '}
                            {selectedLocation.description}
                        </Typography>
                        <Typography variant="body1" gutterBottom>
                            <strong>Email:</strong> {selectedLocation.email}
                        </Typography>
                        <Typography variant="body1" gutterBottom>
                            <strong>Address:</strong>{' '}
                            {selectedLocation.building}{' '}
                            {selectedLocation.street} {selectedLocation.city}{' '}
                            {selectedLocation.country}{' '}
                            {selectedLocation.postalCode}
                        </Typography>
                    </Box>

                    <Box sx={{ py: '6px' }} />
                    <Typography variant="h6" gutterBottom component="div">
                        Devices
                    </Typography>

                    {deviceList.map((item, index) => (
                        <div key={index}>
                            <Typography
                                variant="subtitle1"
                                gutterBottom
                                component="div"
                            >
                                {item.name}
                            </Typography>

                            <Typography
                                variant="body1"
                                gutterBottom
                                component="div"
                            >
                                <strong>Last Check Date:</strong>{' '}
                                {item.lastCheckDate}
                            </Typography>
                            <Typography
                                variant="body1"
                                gutterBottom
                                component="div"
                            >
                                <strong>Warranty Till:</strong>{' '}
                                {item.warrantyTill}
                            </Typography>
                            {/* <Typography
                                variant="body1"
                                gutterBottom
                                component="div"
                            >
                                <strong>workingHours:</strong>{' '}
                                {item.workingHours}
                            </Typography> */}
                            <Box sx={{ py: '6px' }} />
                        </div>
                    ))}
                    <Box sx={{ py: '10px' }} />
                    <Link to="/locations">
                        <Button color="primary" variant="contained">
                            Done
                        </Button>
                    </Link>
                    {/* <Button color="error" variant="contained">
                        Discart changes
                    </Button> */}
                </ValidatorForm>
            </SimpleCard>
        </div>
    )
}
