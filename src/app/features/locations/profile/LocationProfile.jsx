import React, { useState, useEffect } from 'react'
import { ValidatorForm } from 'react-material-ui-form-validator'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { SimpleCard } from 'app/components'
import { useCallback } from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import Divider from '@mui/material/Divider'
import Typography from '@mui/material/Typography'
import Autocomplete from '@mui/material/Autocomplete'
import { ImageButton } from 'app/components/styled/ImageButton'
import { Image } from 'app/components/styled/Image'
import { ImageSrc } from 'app/components/styled/ImageSrc'
import { ImageBackdrop } from 'app/components/styled/ImageBackdrop'
import { Input } from 'app/components/styled/Input'
import { Field } from 'app/components/styled/Field'
import { changeLocationById } from '../LocationsSlice'
import { createLocation } from '../LocationsSlice'

export const LocationProfile = ({ setActiveStep }) => {
    const [state, setState] = useState()
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const { status, countries, cities } = useSelector(
        (state) => state.libraries
    )
    const { selectedLocation, actionStatus } = useSelector(
        (state) => state.locations
    )

    useEffect(() => {
        actionStatus === 'create' &&
            setState({
                ...state,
                countries: countries,
                cities: cities,
            })

        actionStatus === 'created' &&
            setState({
                ...state,
                countries: countries,
                cities: cities,
                ...selectedLocation,
            })

        actionStatus === 'edit' &&
            setState({
                ...state,
                countries: countries,
                cities: cities,
                ...selectedLocation,
            })
    }, [countries, cities, selectedLocation, actionStatus])

    const onCountryChange = (event, value) => {
        setState({
            ...state,
            country: value,
        })
    }

    const onCityChange = (event, value) => {
        setState({
            ...state,
            city: value,
        })
    }

    const handleSubmit = (event) => {
        let newValues = {
            name: state.name,
            description: state.description,
            email: state.email,
            phone: state.phone,
            country: state.country,
            city: state.city,
            street: state.street,
            building: state.building,
            postalCode: state.postalCode,
            address: `${state.street} ${state.building} ${state.city} ${state.country}`,
            devicesNumber: 1,
            employeesNumber: 1,
            membersNumber: 1,
        }
        let selectedValues = {
            ...newValues,
            id: selectedLocation.id,
        }

        actionStatus === 'create' && dispatch(createLocation(newValues))

        actionStatus === 'created' &&
            dispatch(changeLocationById(selectedValues))

        actionStatus === 'edit' && dispatch(changeLocationById(selectedValues))

        actionStatus === 'edit' && navigate('/locations')

        actionStatus === 'create' && setActiveStep(1)

        actionStatus === 'created' && setActiveStep(0)
    }

    const handleChange = (event) => {
        event.persist()
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    return (
        <div>
            {status === 'resolved' && state && (
                <SimpleCard title="Create Location">
                    <ValidatorForm onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item lg={2} md={3} sm={12} xs={12}>
                                <Box
                                    sx={{
                                        display: 'flex',
                                        flexWrap: 'wrap',
                                        minWidth: 300,
                                        width: '100%',
                                    }}
                                >
                                    <label htmlFor="contained-button-file">
                                        <Input
                                            accept="image/*"
                                            id="contained-button-file"
                                            multiple
                                            type="file"
                                        />
                                        <ImageButton
                                            variant="contained"
                                            component="span"
                                            focusRipple
                                            style={{
                                                width: '100px',
                                                height: '100px',
                                            }}
                                        >
                                            <ImageSrc
                                                style={{
                                                    backgroundImage: `url('https://i.pinimg.com/originals/2f/ce/94/2fce94d3f8f3c95bd23faa6256342a91.png')`,
                                                }}
                                            />
                                            <ImageBackdrop className="MuiImageBackdrop-root" />
                                            <Image>
                                                <Typography
                                                    component="span"
                                                    variant="subtitle1"
                                                    color="inherit"
                                                    sx={{
                                                        position: 'relative',
                                                        p: 1,
                                                        pt: 1,
                                                        pb: (theme) =>
                                                            `calc(${theme.spacing(
                                                                1
                                                            )} + 3px)`,
                                                    }}
                                                >
                                                    Upload
                                                </Typography>
                                            </Image>
                                        </ImageButton>
                                    </label>
                                </Box>
                            </Grid>
                            <Grid
                                item
                                lg={4}
                                md={4}
                                sm={12}
                                xs={12}
                                sx={{ mt: 2 }}
                            >
                                <Field
                                    required
                                    name="name"
                                    label="Location Name"
                                    type="text"
                                    onChange={handleChange}
                                    value={state.name || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 35',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 4 symbols',
                                        'should be less than 35 symbols',
                                    ]}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                <Field
                                    required
                                    name="description"
                                    label="Description"
                                    type="text"
                                    multiline
                                    rows="4"
                                    onChange={handleChange}
                                    value={state.description || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 350',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 4 symbols',
                                        'should be less than 350 symbols',
                                    ]}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Field
                                    required
                                    name="email"
                                    label="Email"
                                    type="text"
                                    onChange={handleChange}
                                    value={state.email || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 30',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 4 symbols',
                                        'should be less than 30 symbols',
                                    ]}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Field
                                    required
                                    name="phone"
                                    label="Phone"
                                    type="text"
                                    onChange={handleChange}
                                    value={state.phone || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 30',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 4 symbols',
                                        'should be less than 30 symbols',
                                    ]}
                                />
                            </Grid>
                        </Grid>

                        <Divider>Address</Divider>
                        <Box sx={{ py: '10px' }} />

                        <Grid container spacing={2}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Autocomplete
                                    disablePortal
                                    onChange={onCountryChange}
                                    value={state.country}
                                    isOptionEqualToValue={(option, value) =>
                                        option.value === value.value
                                    }
                                    options={state.countries}
                                    sx={{ width: '100%' }}
                                    validators={['required']}
                                    renderInput={(params) => (
                                        <Field
                                            {...params}
                                            required
                                            name="country"
                                            label="Country"
                                        />
                                    )}
                                />
                                <Field
                                    required
                                    name="street"
                                    label="Street"
                                    type="text"
                                    onChange={handleChange}
                                    value={state.street || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 30',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 4 symbols',
                                        'should be less than 30 symbols',
                                    ]}
                                />
                                <Field
                                    required
                                    name="postalCode"
                                    label="Postal Code"
                                    type="text"
                                    onChange={handleChange}
                                    value={state.postalCode || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 4',
                                        'maxStringLength: 10',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 4 symbols',
                                        'should be less than 10 symbols',
                                    ]}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Autocomplete
                                    disablePortal
                                    id="combo-box-demo"
                                    onChange={onCityChange}
                                    value={state.city}
                                    options={state.cities}
                                    isOptionEqualToValue={(option, value) =>
                                        option.value === value.value
                                    }
                                    sx={{ width: '100%' }}
                                    validators={['required']}
                                    renderInput={(params) => (
                                        <Field
                                            {...params}
                                            required
                                            name="city"
                                            label="City"
                                        />
                                    )}
                                />

                                <Field
                                    required
                                    name="building"
                                    label="Building"
                                    type="text"
                                    onChange={handleChange}
                                    value={state.building || ''}
                                    validators={[
                                        'required',
                                        'minStringLength: 1',
                                        'maxStringLength: 10',
                                    ]}
                                    errorMessages={[
                                        'this field is required',
                                        'should be more than 1 symbols',
                                        'should be less than 10 symbols',
                                    ]}
                                />
                            </Grid>
                        </Grid>
                        <Box sx={{ py: '6px' }} />
                        <Button
                            color="primary"
                            variant="contained"
                            type="submit"
                        >
                            Save changes
                        </Button>
                    </ValidatorForm>
                </SimpleCard>
            )}
        </div>
    )
}
