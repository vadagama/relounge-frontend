import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getAllLocations = createAsyncThunk(
    'locations/getAllLocations',
    async () => {
        const response = await axios.get(`${PREFIX}locations`)
        if (!response) {
            throw new Error("Server Error! Can't get locations.")
        }
        return response.data
    }
)

export const getLocationById = createAsyncThunk(
    'locations/getLocationById',
    async (id) => {
        const response = await axios.get(`${PREFIX}locations/${id}`)
        if (!response) {
            throw new Error(`Server Error! Can't get location №${id}.`)
        }
        return response.data
    }
)

export const createLocation = createAsyncThunk(
    'locations/createLocation',
    async (payload) => {
        const response = await axios.post(`${PREFIX}locations`, payload)
        if (!response) {
            throw new Error(`Server Error! Can't create location.`)
        }
        return response.data
    }
)

export const changeLocationById = createAsyncThunk(
    'locations/changeLocationById',
    async (payload) => {
        const response = await axios.put(
            `${PREFIX}locations/${payload.id}`,
            payload
        )
        if (!response) {
            throw new Error(`Server Error! Can't create location.`)
        }
        return response.data
    }
)

export const deleteLocationById = createAsyncThunk(
    'locations/deleteLocationById',
    async (id) => {
        const response = await axios.delete(`${PREFIX}locations/${id}`)
        if (!response) {
            throw new Error(`Server Error! Can't delete location.`)
        }
        return response.data
    }
)

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    locations: [],
    locationCreated: null,
    selectedLocation: {},
    actionStatus: '',
    payments: [],
    actualStep: 0,
    status: null,
    error: null,
}

const LocationsSlice = createSlice({
    name: 'locations',
    initialState,
    reducers: {
        setActualStep(state, action) {
            state.actualStep = action.payload
        },
        setActionStatus(state, action) {
            state.actionStatus = action.payload
        },
    },
    extraReducers: {
        // Get locations
        [getAllLocations.pending]: setLoading,
        [getAllLocations.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.locations = action.payload
        },
        [getAllLocations.rejected]: setError,

        // Get single location
        [getLocationById.pending]: setLoading,
        [getLocationById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.selectedLocation = action.payload
        },
        [getLocationById.rejected]: setError,

        // Create new location
        [createLocation.pending]: setLoading,
        [createLocation.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.locations.push(action.payload)
            state.selectedLocation = action.payload
            state.actionStatus = 'created'
        },
        [createLocation.rejected]: setError,

        // Change location
        [changeLocationById.pending]: setLoading,
        [changeLocationById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.locations.findIndex(
                (item) => item.id === parseInt(action.payload.id)
            )
            state.locations[itemId] = action.payload
            state.selectedLocation = action.payload
        },
        [changeLocationById.rejected]: setError,

        // Delete location
        [deleteLocationById.pending]: setLoading,
        [deleteLocationById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.locations.findIndex(
                (item) => item.id === parseInt(action.meta.arg)
            )
            state.locations.splice(itemId, 1)
        },
        [deleteLocationById.rejected]: setError,
    },
})

export const { setActionStatus } = LocationsSlice.actions

export default LocationsSlice.reducer
