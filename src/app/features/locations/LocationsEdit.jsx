import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import Tabs from '@mui/material/Tabs'
import Divider from '@mui/material/Divider'
import DeviceSettings from './devices/DeviceSettings'
import { StyledTab } from 'app/components/styled/StyledTab'
import { Container } from 'app/components/styled/Container'
import { Breadcrumb } from 'app/components'
import { LocationProfile } from './profile/LocationProfile'
import { getLocationById } from './LocationsSlice'
import { getCities } from 'app/redux/reducers/LibrariesSlice'
import { getCountries } from 'app/redux/reducers/LibrariesSlice'
import { setActionStatus } from './LocationsSlice'

const LocationsEdit = () => {
    const [tabIndex, setTabIndex] = useState(0)
    const params = useParams()
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setActionStatus('edit'))
    }, [dispatch])

    useEffect(() => {
        dispatch(getLocationById(params.id))
        dispatch(getCities())
        dispatch(getCountries())
    }, [dispatch, params.id])

    const handleTabChange = (e, value) => {
        setTabIndex(value)
    }

    return (
        <Container>
            <div className="breadcrumb">
                <Breadcrumb
                    routeSegments={[
                        { name: 'Locations', path: '/locations' },
                        {
                            name: 'Edit location',
                            path: '/locations/edit',
                        },
                    ]}
                />
            </div>
            <Tabs
                value={tabIndex}
                onChange={handleTabChange}
                indicatorColor="primary"
                textColor="primary"
            >
                {tabList.map((item, ind) => (
                    <StyledTab value={ind} label={item} key={ind} />
                ))}
            </Tabs>
            <Divider />

            {tabIndex === 0 && <LocationProfile />}
            {tabIndex === 1 && <DeviceSettings />}
        </Container>
    )
}

const tabList = ['Location Profile', 'Devices']

export default LocationsEdit
