import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { Box } from '@mui/material'
import { Button } from '@mui/material'
import AddCircleIcon from '@mui/icons-material/AddCircle'
import { Breadcrumb } from 'app/components'
import { Container } from 'app/components/styled/Container'
import { LocationsList } from './LocationsList'
import { getAllLocations } from './LocationsSlice'

const LocationsApp = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    useEffect(() => {
        dispatch(getAllLocations())
    }, [dispatch])

    const handleClick = () => {
        navigate(`create`, { replace: true })
    }

    return (
        <Container>
            <div className="breadcrumb">
                <Breadcrumb
                    routeSegments={[{ name: 'Locations', path: '/locations' }]}
                />
            </div>
            <Button
                variant="contained"
                startIcon={<AddCircleIcon />}
                onClick={handleClick}
            >
                Add a location
            </Button>
            <Box sx={{ py: '6px' }} />
            <LocationsList />
        </Container>
    )
}

export default LocationsApp
