import React from 'react'
import { Box } from '@mui/system'
import { SimpleCard } from 'app/components'
import { LocationsTable } from './LocationsTable'

export const LocationsList = () => {
    return (
        <div>
            <SimpleCard title="Locations">
                <Box width="100%" overflow="auto">
                    <LocationsTable />
                </Box>
            </SimpleCard>
        </div>
    )
}
