import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepButton from '@mui/material/StepButton'
import Box from '@mui/material/Box'
import { Breadcrumb } from 'app/components'
import { Container } from 'app/components/styled/Container'
import { setActionStatus } from './LocationsSlice'
import { LocationProfile } from './profile/LocationProfile'
import { LocationReview } from './LocationReview'
import DeviceSettings from './devices/DeviceSettings'
import { getCountries } from 'app/redux/reducers/LibrariesSlice'
import { getCities } from 'app/redux/reducers/LibrariesSlice'

const LocationsApp = () => {
    const { actionStatus } = useSelector((state) => state.locations)
    const [activeStep, setActiveStep] = React.useState(0)
    const dispatch = useDispatch()
    const steps = [
        'Complete locations profile',
        'Add devices and schedule',
        'Review created location',
    ]

    useEffect(() => {
        dispatch(setActionStatus('create'))
    }, [dispatch])

    useEffect(() => {
        dispatch(getCities())
        dispatch(getCountries())
    }, [dispatch])

    const handleStep = (step) => () => {
        setActiveStep(step)
    }

    return (
        <Container>
            <div className="breadcrumb">
                <Breadcrumb
                    routeSegments={[
                        { name: 'Locations', path: '/locations' },
                        {
                            name: 'Create location',
                            path: '/locations/create',
                        },
                    ]}
                />
            </div>

            <Box sx={{ width: '100%' }}>
                <Stepper nonLinear activeStep={activeStep}>
                    {steps.map((label, index) => (
                        <Step key={label}>
                            {actionStatus === 'created' ? (
                                <StepButton onClick={handleStep(index)}>
                                    {label}
                                </StepButton>
                            ) : (
                                <StepButton>{label}</StepButton>
                            )}
                        </Step>
                    ))}
                </Stepper>
                <Box sx={{ py: '10px' }} />

                {activeStep === 0 && (
                    <LocationProfile setActiveStep={setActiveStep} />
                )}
                {activeStep === 1 && (
                    <DeviceSettings setActiveStep={setActiveStep} />
                )}
                {activeStep === 2 && <LocationReview />}
            </Box>
        </Container>
    )
}

export default LocationsApp
