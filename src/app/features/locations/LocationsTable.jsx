import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import DeleteIcon from '@mui/icons-material/Delete'
import CreateIcon from '@mui/icons-material/Create'
import IconButton from '@mui/material/IconButton'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import Tooltip from '@mui/material/Tooltip'
import TablePagination from '@mui/material/TablePagination'
import Button from '@mui/material/Button'
import { StyledTable } from 'app/components/styled/StyledTable'
import DeleteDialog from './DeleteDialog'
import { deleteLocationById } from './LocationsSlice'

export const LocationsTable = (props) => {
    const dispatch = useDispatch()
    const [rowsPerPage, setRowsPerPage] = React.useState(5)
    const [page, setPage] = React.useState(0)
    const [dialogIsOpen, setDialogIsOpen] = useState(false)
    const [currentItem, setItem] = useState()
    let navigate = useNavigate()

    const { locations, error } = useSelector((state) => state.locations)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value)
        setPage(0)
    }

    const handleViewClick = (event) => {
        event.preventDefault()
        navigate(`${event.currentTarget.value}`, { replace: true })
    }

    const handeDeleteClick = (event) => {
        event.preventDefault()
        setItem(event.currentTarget.value)
        openDialog()
    }

    const openDialog = () => setDialogIsOpen(true)
    const closeDialog = () => setDialogIsOpen(false)

    const deleteLocationsDialog = () => {
        try {
            dispatch(deleteLocationById(currentItem))
            if (error) {
                console.log(error)
            } else {
                setDialogIsOpen(false)
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <>
            <StyledTable>
                <TableHead>
                    <TableRow>
                        <TableCell>Location name</TableCell>
                        <TableCell>Address</TableCell>
                        <TableCell>Employees</TableCell>
                        <TableCell>Devices</TableCell>
                        <TableCell>Members</TableCell>
                        <TableCell width={100}>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {locations
                        .slice(
                            page * rowsPerPage,
                            page * rowsPerPage + rowsPerPage
                        )
                        .map((row) => (
                            <TableRow key={row.id}>
                                <TableCell align="left">
                                    <div>
                                        <Button
                                            sx={{ padding: 0, minWidth: 0 }}
                                            value={row.id}
                                            onClick={handleViewClick}
                                        >
                                            {row.name}
                                        </Button>
                                    </div>
                                </TableCell>
                                <TableCell>{row.address}</TableCell>
                                <TableCell>{row.employeesNumber}</TableCell>
                                <TableCell>{row.devicesNumber}</TableCell>
                                <TableCell>{row.membersNumber}</TableCell>
                                <TableCell>
                                    <IconButton
                                        value={row.id}
                                        onClick={handleViewClick}
                                    >
                                        <Tooltip title="Edit">
                                            <CreateIcon />
                                        </Tooltip>
                                    </IconButton>
                                    <IconButton
                                        value={row.id}
                                        onClick={handeDeleteClick}
                                    >
                                        <Tooltip title="Delete">
                                            <DeleteIcon />
                                        </Tooltip>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </StyledTable>

            <TablePagination
                sx={{ px: 2 }}
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={locations.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page',
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
            <DeleteDialog
                open={dialogIsOpen}
                onDelete={deleteLocationsDialog}
                onClose={closeDialog}
                value={currentItem}
            />
        </>
    )
}
