import React from 'react'
import PropTypes from 'prop-types'
import { Box, Paper, Typography, Button } from '@mui/material'
import Chip from '@mui/material/Chip'
import Stack from '@mui/material/Chip'

function EventItem(props) {
    const {
        type,
        event,
        rowId,
        sx,
        boxSx,
        elevation,
        isMonthMode,
        onClick,
        onMouseOver,
        onDragStart,
    } = props

    const wordsArray = event.trainer.split(' ')
    const firstNamesLetter = Array.from(wordsArray[0])[0]
    const lastNamesLetter = Array.from(wordsArray[1])[0]
    const initials = firstNamesLetter + lastNamesLetter

    return (
        <>
            {type === 'week' ? (
                <Chip
                    onMouseOver={onMouseOver}
                    onClick={onClick}
                    label={initials}
                    sx={{
                        m: '2px',
                        p: 0,
                        backgroundColor: event?.color,
                    }}
                />
            ) : (
                <Paper
                    sx={sx}
                    draggable
                    onClick={onClick}
                    onDragStart={onDragStart}
                    elevation={elevation || 0}
                    key={`item-d-${event?.id}-${rowId}`}
                >
                    <Box sx={boxSx}>
                        <Typography variant="body2" sx={{ fontSize: 11 }}>
                            {event?.label}
                        </Typography>
                        <Typography variant="body2" sx={{ fontSize: 11 }}>
                            {event?.trainer}
                        </Typography>
                        <Typography variant="body2" sx={{ fontSize: 11 }}>
                            {event?.startHour} - {event?.endHour}
                        </Typography>
                    </Box>
                </Paper>
            )}
        </>
    )
}

EventItem.propTypes = {
    sx: PropTypes.object,
    boxSx: PropTypes.object,
    event: PropTypes.object.isRequired,
    rowId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isMonthMode: PropTypes.bool,
    onClick: PropTypes.func,
    handleTaskClick: PropTypes.func,
    onCellDragStart: PropTypes.func,
}

export default EventItem
