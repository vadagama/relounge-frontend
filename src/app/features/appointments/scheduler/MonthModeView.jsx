import React, { useState, useContext } from 'react'
import PropTypes from 'prop-types'
import { format, getDay } from 'date-fns'
import { useTheme, styled, alpha } from '@mui/material/styles'
import {
    Paper,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    tableCellClasses,
    Box,
    Button,
} from '@mui/material'
import { getDaysInMonth, isSameMonth } from 'date-fns'
import EventNoteRoundedIcon from '@mui/icons-material/EventNoteRounded'
import EventItem from './EventItem.jsx'
import { useTranslation } from 'react-i18next'

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        borderTop: `1px ${theme.palette.divider} solid !important`,
        borderBottom: `1px ${theme.palette.divider} solid !important`,
        borderLeft: `1px ${theme.palette.divider} solid !important`,
        ['&:nth-of-type(1)']: {
            borderLeft: `0px !important`,
        },
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 12,
        height: 104,
        width: 64,
        maxWidth: 64,
        cursor: 'pointer',
        verticalAlign: 'center',
        borderLeft: `1px ${theme.palette.divider} solid`,
        ['&:nth-of-type']: {
            borderLeft: 0,
        },
        ['&:nth-of-type(even)']: {
            //backgroundColor: theme.palette.action.hover
        },
    },
    [`&.${tableCellClasses.body}:hover`]: {
        //backgroundColor: "#eee"
    },
}))

const StyledTableCellWeek = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        borderTop: `1px ${theme.palette.divider} solid !important`,
        borderBottom: `1px ${theme.palette.divider} solid !important`,
        borderLeft: `1px ${theme.palette.divider} solid !important`,
        ['&:nth-of-type(1)']: {
            borderLeft: `0px !important`,
        },
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 11,
        height: 104,
        width: 30,
        verticalAlign: 'center',
        borderLeft: `0px ${theme.palette.divider} solid`,
        ['&:nth-of-type']: {
            borderLeft: 0,
        },
        ['&:nth-of-type(even)']: {
            //backgroundColor: theme.palette.action.hover
        },
    },
    [`&.${tableCellClasses.body}:hover`]: {
        //backgroundColor: "#eee"
    },
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    ['&:last-child td, &:last-child th']: {
        border: 0,
    },
}))

function MonthModeView(props) {
    const { rows, locale, options, legacyStyle, columns, date } = props
    const theme = useTheme()
    const [state, setState] = useState({})
    const { t } = useTranslation(['common'])
    const today = new Date()
    let currentDaySx = {
        width: 24,
        height: 24,
        margin: 'auto',
        display: 'block',
        paddingTop: '2px',
        borderRadius: '50%',
    }

    console.log(props)

    const renderTask = (tasks = [], rowId) => {
        return (
            <Button onClick={(e) => handleTaskClick(e, tasks)}>
                <Typography variant="h5">{tasks.length}</Typography>
            </Button>
        )
    }

    const handleTaskClick = (event, tasks) => {
        event.preventDefault()
        event.stopPropagation()
    }

    const handleDateChange = (event, row) => {
        event.preventDefault()
        event.stopPropagation()
        console.log(row)
    }

    return (
        <TableContainer component={Paper} sx={{ boxShadow: 'none' }}>
            <Table
                size="small"
                aria-label="simple table"
                stickyHeader
                sx={{ minWidth: options?.minWidth || 650 }}
            >
                <TableHead sx={{ height: 24 }}>
                    <StyledTableRow>
                        <StyledTableCellWeek></StyledTableCellWeek>
                        {columns?.map((column, index) => (
                            <StyledTableCell
                                align="center"
                                key={column?.headerName + '-' + index}
                            >
                                {column?.headerName}
                            </StyledTableCell>
                        ))}
                    </StyledTableRow>
                </TableHead>
                <TableBody>
                    {rows?.map((row, index) => (
                        <>
                            <StyledTableRow
                                key={`row-${row.id}-${index}`}
                                sx={{
                                    '&:last-child th': {
                                        borderLeft: `1px ${theme.palette.divider} solid`,
                                        '&:first-child': {
                                            borderLeft: 0,
                                        },
                                    },
                                }}
                            >
                                <StyledTableCellWeek
                                    align="center"
                                    component="th"
                                    sx={{
                                        position: 'relative',
                                    }}
                                >
                                    <Button onClick={handleDateChange}>
                                        {index + 1}
                                    </Button>
                                </StyledTableCellWeek>
                                {row?.days?.map((day, indexD) => {
                                    const currentDay =
                                        day.day === today.getUTCDate() &&
                                        isSameMonth(day.date, today)
                                    return (
                                        <StyledTableCell
                                            scope="row"
                                            align="center"
                                            component="th"
                                            sx={{
                                                px: 0.5,
                                                position: 'relative',
                                            }}
                                            key={`day-${day.id}`}
                                        >
                                            <Box
                                                sx={{
                                                    height: '100%',
                                                    overflowY: 'visible',
                                                }}
                                            >
                                                <Typography
                                                    variant="body2"
                                                    sx={{
                                                        ...currentDaySx,
                                                        background:
                                                            currentDay &&
                                                            alpha(
                                                                theme.palette
                                                                    .primary
                                                                    .main,
                                                                1
                                                            ),
                                                        color:
                                                            currentDay &&
                                                            '#fff',
                                                    }}
                                                >
                                                    {day.day}
                                                </Typography>
                                                {day?.data?.length > 0 &&
                                                    renderTask(
                                                        day?.data,
                                                        row.id
                                                    )}
                                            </Box>
                                        </StyledTableCell>
                                    )
                                })}
                            </StyledTableRow>
                        </>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

MonthModeView.propTypes = {
    columns: PropTypes.array,
    rows: PropTypes.array,
    date: PropTypes.string,
    options: PropTypes.object,
    onDateChange: PropTypes.func,
    onTaskClick: PropTypes.func,
    onCellClick: PropTypes.func,
}

MonthModeView.defaultProps = {
    columns: [],
    rows: [],
}

export default MonthModeView
