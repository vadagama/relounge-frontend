import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getAllAppointments = createAsyncThunk(
    'appointments/getAllAppointments',
    async () => {
        const response = await axios.get(`${PREFIX}appointments`)
        return response.data
    }
)

export const getAppointmentById = createAsyncThunk(
    'appointments/getAppointmentById',
    async (id) => {
        const response = await axios.get(`${PREFIX}appointments/${id}`)
        return response.data
    }
)

export const createAppointment = createAsyncThunk(
    'appointments/createAppointment',
    async (payload) => {
        const response = await axios.post(`${PREFIX}appointments`, payload)
        return response.data
    }
)

export const changeAppointmentById = createAsyncThunk(
    'appointments/changeAppointmentById',
    async (payload) => {
        const response = await axios.put(
            `${PREFIX}appointments/${payload.id}`,
            payload
        )
        return response.data
    }
)

export const deleteAppointmentById = createAsyncThunk(
    'appointments/deleteAppointmentById',
    async (id) => {
        const response = await axios.delete(`${PREFIX}appointments/${id}`)
        return response.data
    }
)

export const getDevices = createAsyncThunk('devices/getDevices', async () => {
    const response = await axios.get(`${PREFIX}device`)
    if (!response) {
        throw new Error("Server Error! Can't get devices.")
    }
    return response.data
})

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    appointments: [],
    blockedDevices: [],
    devices: [],
    devicesListWithBlocked: [],
    selectedAppointment: {},
    error: null,
    status: null,
}

export const AppointmentsSlice = createSlice({
    name: 'appointments',
    initialState,
    reducers: {},
    extraReducers: {
        // Get appointments list
        [getAllAppointments.pending]: setLoading,
        [getAllAppointments.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.appointments = action.payload
        },
        [getAllAppointments.rejected]: setError,

        // Get single appointment
        [getAppointmentById.pending]: setLoading,
        [getAppointmentById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.selectedAppointment = action.payload
            state.blockedDevices = action.payload.blockedDevices
        },
        [getAppointmentById.rejected]: setError,

        // Create new appointment
        [createAppointment.pending]: setLoading,
        [createAppointment.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.appointments.push(action.payload)
            state.selectedAppointment = action.payload
        },
        [createAppointment.rejected]: setError,

        // Change appointment
        [changeAppointmentById.pending]: setLoading,
        [changeAppointmentById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.appointments.findIndex(
                (item) => item.id === parseInt(action.payload.id)
            )
            state.appointments[itemId] = action.payload
            state.selectedAppointment = action.payload
        },
        [changeAppointmentById.rejected]: setError,

        // Delete appointment
        [deleteAppointmentById.pending]: setLoading,
        [deleteAppointmentById.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const itemId = state.appointments.findIndex(
                (item) => item.id === parseInt(action.meta.arg)
            )
            state.appointments.splice(itemId, 1)
        },
        [deleteAppointmentById.rejected]: setError,

        // Get devices list
        [getDevices.pending]: setLoading,
        [getDevices.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.devices = action.payload
            state.devicesListWithBlocked = [
                action.payload,
                state.blockedDevices,
            ]
        },
        [getDevices.rejected]: setError,
    },
})

export default AppointmentsSlice.reducer
