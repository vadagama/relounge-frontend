import React from 'react'
import * as ReactDOM from 'react-dom'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { useMemo } from 'react'
import { useRef } from 'react'
import { useEffect } from 'react'
import format from 'date-fns/format'
import parse from 'date-fns/parse'
import startOfWeek from 'date-fns/startOfWeek'
import getDay from 'date-fns/getDay'
import enUS from 'date-fns/locale/en-US'
import addHours from 'date-fns/addHours'
import startOfHour from 'date-fns/startOfHour'
import { Breadcrumb } from 'app/components'
import Button from '@mui/material/Button'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import { SimpleCard } from 'app/components'
import { Container } from 'app/components/styled/Container'
import AddCircleIcon from '@mui/icons-material/AddCircle'
import { getAllAppointments } from './AppointmentsSlice'
import { changeAppointmentById } from './AppointmentsSlice'
import { getDevices } from '../locations/devices/DevicesSlice'
import Scheduler from './scheduler/Scheduler'

import { styled } from '@mui/system'
import TextField from '@mui/material/TextField'
import FormLabel from '@mui/material/FormLabel'
import FormControl from '@mui/material/FormControl'
import FormGroup from '@mui/material/FormGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormHelperText from '@mui/material/FormHelperText'
import Checkbox from '@mui/material/Checkbox'

// import { Field } from '../../components/styled/Field'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { CalendarPicker } from '@mui/x-date-pickers/CalendarPicker'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'

const Field = styled(TextField)(() => ({
    width: '100%',
    marginBottom: '16px',
}))

const date = new Date()

const AppointmentTypeFilter = ({ calendarDate, setCalendarDate }) => {
    const [state, setState] = useState()

    const handleChange = (event) => {
        setState({
            ...state,
        })
    }

    const handleDateChange = (date) => {
        setState({
            ...state,
            date: date,
        })
        setCalendarDate(date)
    }

    return (
        <>
            <Stack
                direction="column"
                justifyContent="flex-start"
                alignItems="flex-start"
                spacing={0}
                sx={{ width: 200 }}
            >
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <CalendarPicker
                        sx={{ p: 0, m: 0 }}
                        date={new Date(calendarDate)}
                        onChange={(date) => handleDateChange(date)}
                    />
                </LocalizationProvider>

                <FormControl component="fieldset" variant="standard">
                    <FormLabel>Type of appointment</FormLabel>
                    <FormGroup>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={handleChange}
                                    name="Anamnesis"
                                    sx={{
                                        color: '#099ce5',
                                        '&.Mui-checked': {
                                            color: '#099ce5',
                                        },
                                    }}
                                />
                            }
                            label="Anamnesis"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={handleChange}
                                    name="Reckeck"
                                    sx={{
                                        color: '#f28f6a',
                                        '&.Mui-checked': {
                                            color: '#f28f6a',
                                        },
                                    }}
                                />
                            }
                            label="Reckeck"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={handleChange}
                                    name="Backcheck"
                                    sx={{
                                        color: 'green',
                                        '&.Mui-checked': {
                                            color: 'green',
                                        },
                                    }}
                                />
                            }
                            label="Backcheck"
                        />
                    </FormGroup>
                    {/* <FormHelperText>
                                        Be careful
                                    </FormHelperText> */}
                </FormControl>

                <Box sx={{ p: '10px' }}></Box>

                <FormControl component="fieldset" variant="standard">
                    <FormLabel component="legend">Trainers</FormLabel>
                    <FormGroup>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={handleChange}
                                    name="Johny Depp"
                                />
                            }
                            label="Johny Depp"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={handleChange}
                                    name="Mike Tyson"
                                />
                            }
                            label="Mike Tyson"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={handleChange}
                                    name="Susan Boil"
                                />
                            }
                            label="Susan Boil"
                        />
                    </FormGroup>
                    {/* <FormHelperText>
                                        Be careful
                                    </FormHelperText> */}
                </FormControl>
            </Stack>
        </>
    )
}

const AppointmentsApp = () => {
    const [state] = useState({
        options: {
            transitionMode: 'none', // or fade
            startWeekOn: 'mon', // or sun
            defaultMode: 'day', // or week | day | timeline
            minWidth: 540,
            maxWidth: 540,
            minHeight: 540,
            maxHeight: 540,
        },
        toolbarProps: {
            showSearchBar: false,
            showSwitchModeButtons: true,
            showDatePicker: true,
        },
    })
    const [newEvent, setNewEvent] = useState(null)
    const [calendarDate, setCalendarDate] = useState(Date.now())
    const [shouldShowEventDialog, setShouldShowEventDialog] = useState(false)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllAppointments())
        dispatch(getDevices())
    }, [dispatch])

    const { appointments } = useSelector((state) => state.appointments)

    const devices = [
        {
            id: 0,
            name: 'Device 3bde71d',
        },
        {
            id: 1,
            name: 'Device b91f44f',
        },
        {
            id: 2,
            name: 'Device 279ab35',
        },
    ]

    const events = [
        {
            id: 'event-1',
            label: 'Anamnesys',
            groupLabel: 'Dr Shaun Murphy',
            user: 'Dr Shaun Murphy',
            appointmentType: 'Anamnesys',
            trainer: 'Johny Depp',
            deviceId: 0,
            color: '#099ce5',
            startHour: '09:00 AM',
            endHour: '10:00 AM',
            date: '2022-08-03',
            createdAt: new Date(),
            createdBy: 'Kristina Mayer',
        },
        {
            id: 'event-2',
            label: 'Anamnesis',
            groupLabel: 'Dr Claire Brown',
            user: 'Dr Claire Brown',
            appointmentType: 'Reckeck',
            trainer: 'Mike Tyson',
            deviceId: 0,
            color: '#099ce5',
            startHour: '09:00 AM',
            endHour: '10:00 AM',
            date: '2022-08-01',
            createdAt: new Date(),
            createdBy: 'Kristina Mayer',
        },
        {
            id: 'event-3',
            label: 'Reckeck',
            groupLabel: 'Dr Claire Brown',
            user: 'Dr Claire Brown',
            appointmentType: 'Reckeck',
            trainer: 'Johny Depp',
            deviceId: 1,
            color: '#f28f6a',
            startHour: '09:00 AM',
            endHour: '10:00 AM',
            date: '2022-08-01',
            createdAt: new Date(),
            createdBy: 'Kristina Mayer',
        },
        {
            id: 'event-4',
            label: 'Reckeck',
            groupLabel: 'Dr Claire Brown',
            user: 'Dr Claire Brown',
            appointmentType: 'Reckeck',
            trainer: 'Susan Boil',
            deviceId: 1,
            color: '#f28f6a',
            startHour: '11:00 AM',
            endHour: '12:00 AM',
            date: '2022-08-03',
            createdAt: new Date(),
            createdBy: 'Kristina Mayer',
        },
    ]

    console.log(appointments)

    const handleCellClick = (event, row, day) => {
        // Do something...
    }

    const handleEventClick = (event, item) => {
        // Do something...
    }

    const handleEventsChange = (item) => {}

    const handleAlertCloseButtonClicked = (item) => {
        // Do something...
    }

    const handleDialogClose = () => {
        setShouldShowEventDialog(false)
    }

    const handleEventMove = (event) => {
        const ev = {
            ...event.event,
            start: event.start,
            end: event.end,
        }
        dispatch(changeAppointmentById(ev))
    }

    const handleEventResize = (event) => {
        const ev = {
            ...event.event,
            start: event.start,
            end: event.end,
        }
        dispatch(changeAppointmentById(ev))
    }

    const handleDateChange = () => {}

    const openNewEventDialog = (event) => {
        console.log(event, 'new')
        setNewEvent({
            ...event,
            date: Date.now(),
            startTime: '09:00',
            endTime: '09:30',
        })
        setShouldShowEventDialog(true)
    }

    const openExistingEventDialog = (event) => {
        console.log(event, 'edit')
        setNewEvent(event)
        setShouldShowEventDialog(true)
    }

    return (
        <div>
            <Container>
                <div className="breadcrumb">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Appointments', path: '/appointments' },
                        ]}
                    />
                </div>
                {/* <div>
                    <Button
                        variant="contained"
                        startIcon={<AddCircleIcon />}
                        onClick={() =>
                            openNewEventDialog({
                                start: new Date(),
                                end: new Date(),
                                AppointmentType: 'Anamnesis',
                                employee: 'Johny',
                            })
                        }
                    >
                        Block time
                    </Button>
                </div>
                <Box sx={{ py: '6px' }} /> */}
                <SimpleCard title="Appointments">
                    <Grid
                        container
                        spacing={2}
                        justifyContent="flex-start"
                        direction="row"
                    >
                        <Grid
                            item
                            lg={3}
                            md={12}
                            sm={12}
                            xs={12}
                            sx={{ minWidth: '330px' }}
                        >
                            <AppointmentTypeFilter
                                calendarDate={calendarDate}
                                setCalendarDate={setCalendarDate}
                            />
                        </Grid>
                        <Grid item lg={8} md={12} sm={12} xs={12}>
                            <Box width="100%" overflow="auto">
                                <Scheduler
                                    locale="en"
                                    events={events}
                                    devices={devices}
                                    date={state.date}
                                    legacyStyle={false}
                                    options={state?.options}
                                    alertProps={state?.alertProps}
                                    toolbarProps={state?.toolbarProps}
                                    onEventsChange={handleEventsChange}
                                    onCellClick={handleCellClick}
                                    onTaskClick={handleEventClick}
                                    onAlertCloseButtonClicked={
                                        handleAlertCloseButtonClicked
                                    }
                                />
                            </Box>
                        </Grid>
                    </Grid>
                </SimpleCard>
            </Container>
        </div>
    )
}

export default AppointmentsApp
