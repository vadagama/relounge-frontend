import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getAllInvoices = createAsyncThunk(
    'finance/getAllInvoices',
    async () => {
        const response = await axios.get(`${PREFIX}invoices`)
        if (!response) {
            throw new Error("Server Error! Can't get invoices.")
        }
        return response.data
    }
)

export const searchInvoices = createAsyncThunk(
    'finance/searchInvoices',
    async (payload) => {
        const response = await axios.get(`${PREFIX}invoices?q=${payload}`)
        if (!response) {
            throw new Error("Server Error! Can't get invoices.")
        }
        return response.data
    }
)

export const filterInvoicesByDate = createAsyncThunk(
    'finance/filterInvoicesByDate',
    async (payload) => {
        const response = await axios.get(`${PREFIX}invoices?date=${payload}`)
        if (!response) {
            throw new Error("Server Error! Can't get invoices.")
        }
        return response.data
    }
)

export const getAllPayments = createAsyncThunk(
    'finance/getAllPayments',
    async () => {
        const response = await axios.get(`${PREFIX}payments`)
        if (!response) {
            throw new Error("Server Error! Can't get payments.")
        }
        return response.data
    }
)

export const searchPayments = createAsyncThunk(
    'finance/searchPayments',
    async (payload) => {
        const response = await axios.get(`${PREFIX}payments?q=${payload}`)
        if (!response) {
            throw new Error("Server Error! Can't get payments.")
        }
        return response.data
    }
)

export const filterPaymentsByDate = createAsyncThunk(
    'finance/filterPaymentsByDate',
    async (payload) => {
        const response = await axios.get(`${PREFIX}payments?date=${payload}`)
        if (!response) {
            throw new Error("Server Error! Can't get payments.")
        }
        return response.data
    }
)

export const getInvoiceTemplate = createAsyncThunk(
    'finance/getInvoiceTemplate',
    async () => {
        const response = await axios.get(`${PREFIX}templateSettings/0`)
        return response.data
    }
)

export const changeInvoiceTemplate = createAsyncThunk(
    'finance/changeInvoiceTemplate',
    async (payload) => {
        console.log(payload)
        const response = await axios.put(`${PREFIX}templateSettings/0`, payload)
        return response.data
    }
)

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    invoices: [],
    payments: [],
    dateFilterValue: '',
    invoiceTemplate: null,
    status: null,
    error: null,
}

const FinanceSlice = createSlice({
    name: 'finance',
    initialState,
    reducers: {
        setDateFilterValue(state, action) {
            state.dateFilterValue = action.payload
        },
    },
    extraReducers: {
        // Get invoices extra reducers
        [getAllInvoices.pending]: setLoading,
        [getAllInvoices.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.invoices = action.payload
            state.error = null
        },
        [getAllInvoices.rejected]: setError,

        // Search invoices extra reducers
        [searchInvoices.pending]: setLoading,
        [searchInvoices.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.dateFilterValue
                ? (state.invoices = action.payload.filter(
                      (item) => item.date === state.dateFilterValue
                  ))
                : (state.invoices = action.payload)

            state.error = null
        },
        [searchInvoices.rejected]: setError,

        // Filter invoices by date extra reducers
        [filterInvoicesByDate.pending]: setLoading,
        [filterInvoicesByDate.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.invoices = action.payload
            state.error = null
        },
        [filterInvoicesByDate.rejected]: setError,

        // Get payments extra reducers
        [getAllPayments.pending]: setLoading,
        [getAllPayments.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.payments = action.payload
            state.error = null
        },
        [getAllPayments.rejected]: setError,

        // Search payments extra reducers
        [searchPayments.pending]: setLoading,
        [searchPayments.fulfilled]: (state, action) => {
            state.status = 'resolved'

            state.dateFilterValue
                ? (state.payments = action.payload.filter(
                      (item) => item.date === state.dateFilterValue
                  ))
                : (state.payments = action.payload)

            state.error = null
        },
        [searchPayments.rejected]: setError,

        // Filter payments by date extra reducers
        [filterPaymentsByDate.pending]: setLoading,
        [filterPaymentsByDate.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.payments = action.payload
            state.error = null
        },
        [filterPaymentsByDate.rejected]: setError,

        // Get invoice template extra reducers
        [getInvoiceTemplate.pending]: setLoading,
        [getInvoiceTemplate.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.invoiceTemplate = action.payload
            state.error = null
        },
        [getInvoiceTemplate.rejected]: setError,

        // Change invoice template extra reducers
        [changeInvoiceTemplate.pending]: setLoading,
        [changeInvoiceTemplate.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.invoiceTemplate = action.payload
            state.error = null
        },
        [changeInvoiceTemplate.rejected]: setError,
    },
})

export const { setDateFilterValue } = FinanceSlice.actions

export default FinanceSlice.reducer
