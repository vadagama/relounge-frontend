import * as React from 'react'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import TablePagination from '@mui/material/TablePagination'
import Stack from '@mui/material/Stack'
import IconButton from '@mui/material/IconButton'
import Tooltip from '@mui/material/Tooltip'
import DownloadIcon from '@mui/icons-material/Download'
import FilterIcon from '@mui/icons-material/FilterList'
import { SimpleCard } from 'app/components'
import { FinanceFilters } from 'app/components'
import { searchPayments } from './FinanceSlice'
import { filterPaymentsByDate } from './FinanceSlice'
import { setDateFilterValue } from './FinanceSlice'
import { getAllPayments } from './FinanceSlice'

const FinancePayments = () => {
    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)
    const { payments } = useSelector((state) => state.finance)
    const [isVisible, setVisible] = React.useState(false)
    const dispatch = useDispatch()

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value)
        setPage(0)
    }

    const handleShowFilter = () => {
        setVisible(true)
    }

    return (
        <>
            <SimpleCard title="Payments">
                {isVisible ? (
                    <FinanceFilters
                        setVisible={setVisible}
                        isVisible={isVisible}
                        setDateFilterValue={(value) => {
                            dispatch(setDateFilterValue(value))
                        }}
                        filterItemsByDate={(value) => {
                            dispatch(filterPaymentsByDate(value))
                        }}
                        searchItems={(value) => {
                            dispatch(searchPayments(value))
                        }}
                        getAllItems={() => {
                            dispatch(getAllPayments())
                        }}
                    />
                ) : (
                    <Stack direction="row" justifyContent="flex-end">
                        <IconButton
                            onClick={handleShowFilter}
                            edge="end"
                            sx={{
                                width: 50,
                                height: 50,
                                position: 'absolute',
                                marginRight: '5px',
                                marginTop: '-50px',
                            }}
                        >
                            <FilterIcon />
                        </IconButton>
                    </Stack>
                )}
                <TableContainer>
                    <Table size="small" aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Service Name</TableCell>
                                <TableCell>Member Name</TableCell>
                                <TableCell>Payment Date</TableCell>
                                <TableCell>Invoice No</TableCell>
                                <TableCell>Amount</TableCell>
                                <TableCell sx={{ width: '50px' }}></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {payments
                                .slice(
                                    page * rowsPerPage,
                                    page * rowsPerPage + rowsPerPage
                                )
                                .map((row) => (
                                    <TableRow key={row.id}>
                                        <TableCell component="th" scope="row">
                                            {row.serviceName}
                                        </TableCell>
                                        <TableCell>{row.member}</TableCell>
                                        <TableCell>{row.date}</TableCell>
                                        <TableCell>
                                            {row.invoiceNumber}
                                        </TableCell>
                                        <TableCell>$ {row.amount}</TableCell>
                                        <TableCell>
                                            <a
                                                href={row.file}
                                                target="_blank"
                                                rel="noreferrer"
                                            >
                                                <IconButton>
                                                    <Tooltip title="Download">
                                                        <DownloadIcon />
                                                    </Tooltip>
                                                </IconButton>
                                            </a>
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={payments.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </SimpleCard>
        </>
    )
}

export default FinancePayments
