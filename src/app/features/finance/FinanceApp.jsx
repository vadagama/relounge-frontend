import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Breadcrumb } from 'app/components'
import { Container } from 'app/components/styled/Container'
import FinanceInvoices from './FinanceInvoices'
import FinancePayments from './FinancePayments'
import { ReTabs } from 'app/components/styled/ReTabs'
import { getAllInvoices } from './FinanceSlice'
import { getAllPayments } from './FinanceSlice'

const FinanceApp = () => {
    const [tabIndex, setTabIndex] = useState(0)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllInvoices())
        dispatch(getAllPayments())
    }, [dispatch])

    return (
        <Container>
            <Breadcrumb
                routeSegments={[{ name: 'Finance', path: '/finance' }]}
            />
            <ReTabs
                tabList={tabList}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
            />
            {tabIndex === 0 && <FinanceInvoices />}
            {tabIndex === 1 && <FinancePayments />}
        </Container>
    )
}

const tabList = ['Invoices', 'Payments']

export default FinanceApp
