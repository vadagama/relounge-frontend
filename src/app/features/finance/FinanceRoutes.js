import React, { lazy } from 'react'
import Loadable from 'app/components/Loadable/Loadable'
import { authRoles } from '../../auth/authRoles'

const FinanceApp = Loadable(lazy(() => import('./FinanceApp')))

const FinanceRoutes = [
    {
        path: '/finance',
        element: <FinanceApp />,
        auth: authRoles.admin,
    },
]

export default FinanceRoutes
