import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormHelperText from '@mui/material/FormHelperText'
import FormControl from '@mui/material/FormControl'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import { Icon, IconButton } from '@mui/material'
import { useEffect } from 'react'
import { getLocations } from 'app/redux/reducers/LibrariesSlice'

export const LocationSelector = () => {
    const [state, setState] = React.useState()
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getLocations())
    }, [dispatch])

    useEffect(() => {
        setState(locations)
    }, [locations])

    const { locations } = useSelector((state) => state.libraries)

    const handleChange = (event) => {
        setState(event.target.value)
    }

    console.log(locations)

    return (
        <>
            <FormControl sx={{ minWidth: 100 }} size="small">
                <Select value={state} onChange={handleChange}>
                    {locations.map((item, index) => (
                        <MenuItem key={index} value={item}>
                            {item}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </>
    )
}
