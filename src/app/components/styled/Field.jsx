import { styled } from '@mui/system'
import { TextValidator } from 'react-material-ui-form-validator'

export const Field = styled(TextValidator)(() => ({
    width: '100%',
    paddingBottom: '16px',
}))