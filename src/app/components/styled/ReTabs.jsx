import React from 'react'
import { styled } from '@mui/system'
import Divider from '@mui/material/Divider'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'

const StyledTab = styled(Tab)(({ theme }) => ({
    textTransform: 'none',
}))

export const ReTabs = ({ tabList, tabIndex, setTabIndex }) => {
    const handleTabChange = (e, value) => {
        setTabIndex(value)
    }

    return (
        <>
            <Tabs
                value={tabIndex}
                onChange={handleTabChange}
                indicatorColor="primary"
                textColor="primary"
            >
                {tabList.map((item, ind) => (
                    <StyledTab value={ind} label={item} key={ind} />
                ))}
            </Tabs>
            <Divider sx={{ marginBottom: '10px' }} />
        </>
    )
}
