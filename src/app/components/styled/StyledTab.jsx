import Tab from '@mui/material/Tab';
import { styled } from '@mui/system'

export const StyledTab = styled(Tab)(({ theme }) => ({
    textTransform: 'none',
}))