import * as React from 'react'
import { format } from 'date-fns'
import TextField from '@mui/material/TextField'
import FormControl from '@mui/material/FormControl'
import OutlinedInput from '@mui/material/OutlinedInput'
import Stack from '@mui/material/Stack'
import IconButton from '@mui/material/IconButton'
import SearchIcon from '@mui/icons-material/Search'
import ClearIcon from '@mui/icons-material/Clear'
import FilterListOffIcon from '@mui/icons-material/FilterListOff'
import InputAdornment from '@mui/material/InputAdornment'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'

export const FinanceFilters = ({
    setVisible,
    setDateFilterValue,
    filterItemsByDate,
    searchItems,
    getAllItems,
}) => {
    const [value, setValue] = React.useState('')
    const [date, setDate] = React.useState(null)
    const [showCross, setShowCross] = React.useState(false)

    const handleDateChange = (newValue) => {
        try {
            setDate(newValue)
            setDateFilterValue(format(newValue, 'MM/dd/yyyy'))
            filterItemsByDate(format(newValue, 'MM/dd/yyyy'))
            setValue('')
        } catch {
            console.log(newValue)
        }
    }

    const handleChange = (event) => {
        setValue(event.target.value)
        setShowCross(true)
        searchItems(event.target.value)
        if (event.target.value === '') setShowCross(false)
    }

    const handleClick = (event) => {
        event.preventDefault()
        setValue('')
        searchItems('')
        setShowCross(false)
    }

    const handleMouseDown = (event) => {
        event.preventDefault()
    }

    const handleFilterOff = () => {
        setVisible(false)
        setDate(null)
        setDateFilterValue(null)
        getAllItems()
    }

    return (
        <>
            <Stack direction="row" spacing={2} sx={{ paddingBottom: '10px' }}>
                <FormControl sx={{ width: '100%' }} variant="outlined">
                    <OutlinedInput
                        value={value}
                        onChange={handleChange}
                        placeholder="Search member or invoice"
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    onClick={handleClick}
                                    onMouseDown={handleMouseDown}
                                    edge="end"
                                >
                                    {showCross ? <ClearIcon /> : <SearchIcon />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DesktopDatePicker
                        label="Filter by date"
                        inputFormat="MM/dd/yyyy"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => (
                            <TextField {...params} sx={{ width: 250 }} />
                        )}
                    />
                </LocalizationProvider>
                <IconButton
                    aria-label="delete"
                    sx={{ width: 50 }}
                    onClick={handleFilterOff}
                >
                    <FilterListOffIcon />
                </IconButton>
            </Stack>
        </>
    )
}
