import React, { useState } from 'react'
import { Icon, IconButton } from '@mui/material'
import { Button } from '@mui/material'
import { BloodtypeOutlined, TextFormat } from '@mui/icons-material'

export const LanguageSelector = () => {
    return (
        <>
            <div>
                <Button
                    sx={{
                        color: '#000',
                        textDecoration: 'underline',
                        margin: '0px',
                        padding: '0px',
                        minWidth: 0,
                        verticalAlign: 0,
                    }}
                >
                    EN
                </Button>{' '}
                /{' '}
                <span>
                    <strong>DE</strong>
                </span>
            </div>
        </>
    )
}
