import React, { useState, useEffect } from 'react'
import FormControl from '@mui/material/FormControl'
import OutlinedInput from '@mui/material/OutlinedInput'
import Stack from '@mui/material/Stack'
import IconButton from '@mui/material/IconButton'
import SearchIcon from '@mui/icons-material/Search'
import ClearIcon from '@mui/icons-material/Clear'
import ListItemText from '@mui/material/ListItemText'
import Checkbox from '@mui/material/Checkbox'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import FilterListOffIcon from '@mui/icons-material/FilterListOff'
import InputAdornment from '@mui/material/InputAdornment'

// Multiselect control
const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
}

export const EmployeesFilters = ({
    setVisible,
    filterItemsByLocations,
    locations,
    searchItems,
    getAllItems,
}) => {
    const [value, setValue] = React.useState('')
    const [selectedLocations, setSelectedLocations] = React.useState([])
    const [showCross, setShowCross] = React.useState(false)

    const handleChange = (event) => {
        setValue(event.target.value)
        setShowCross(true)
        searchItems(event.target.value)
        if (event.target.value === '') setShowCross(false)
    }

    const handleLocationsSelect = (event) => {
        const {
            target: { value },
        } = event

        setSelectedLocations(value)
        if (value.length > 0) {
            filterItemsByLocations(value)
        } else {
            getAllItems()
        }
    }

    const handleClick = (event) => {
        event.preventDefault()
        setValue('')
        searchItems('')
        setShowCross(false)
    }

    const handleMouseDown = (event) => {
        event.preventDefault()
    }

    const handleFilterOff = () => {
        setVisible(false)
        getAllItems()
    }

    return (
        <>
            <Stack direction="row" spacing={2} sx={{ paddingBottom: '10px' }}>
                <FormControl sx={{ width: '100%' }} variant="outlined">
                    <OutlinedInput
                        value={value}
                        onChange={handleChange}
                        placeholder="Search member or invoice"
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    onClick={handleClick}
                                    onMouseDown={handleMouseDown}
                                    edge="end"
                                >
                                    {showCross ? <ClearIcon /> : <SearchIcon />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <FormControl
                    sx={{
                        width: '100%',
                        paddingBottom: '16px',
                    }}
                >
                    <InputLabel id="demo-multiple-checkbox-label">
                        Locations
                    </InputLabel>
                    <Select
                        multiple
                        value={selectedLocations}
                        onChange={handleLocationsSelect}
                        input={<OutlinedInput label="Locations" />}
                        renderValue={(selected) => selected.join(', ')}
                        MenuProps={MenuProps}
                    >
                        {locations.map((location, index) => (
                            <MenuItem key={location} value={location}>
                                <Checkbox
                                    checked={
                                        selectedLocations.indexOf(location) > -1
                                    }
                                />
                                <ListItemText primary={location} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <IconButton
                    aria-label="delete"
                    sx={{ width: 50 }}
                    onClick={handleFilterOff}
                >
                    <FilterListOffIcon />
                </IconButton>
            </Stack>
        </>
    )
}
