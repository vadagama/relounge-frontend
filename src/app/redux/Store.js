// import thunk from 'redux-thunk'
// import { createStore, applyMiddleware, compose } from 'redux'
// import RootReducer from './reducers/RootReducer'
import { configureStore } from '@reduxjs/toolkit'
import AppointmentsSlice from 'app/features/appointments/AppointmentsSlice'
import EmployeesSlice from 'app/features/employees/EmployeesSlice'
import FinanceSlice from 'app/features/finance/FinanceSlice'
import DevicesSlice from 'app/features/locations/devices/DevicesSlice'
import LocationsSlice from 'app/features/locations/LocationsSlice'
import MembersSlice from 'app/features/members/MembersSlice'
import SettingsSlice from 'app/features/settings/SettingsSlice'
import LibrariesSlice from './reducers/LibrariesSlice'

export const store = configureStore({
    reducer: {
        libraries: LibrariesSlice,
        employees: EmployeesSlice,
        members: MembersSlice,
        finance: FinanceSlice,
        settings: SettingsSlice,
        locations: LocationsSlice,
        devices: DevicesSlice,
        appointments: AppointmentsSlice,
    },
})

// const initialState = {}
// const middlewares = [thunk]
// let devtools = (x) => x

// if (
//     process &&
//     process.env.NODE_ENV !== 'production' &&
//     process.browser &&
//     window.__REDUX_DEVTOOLS_EXTENSION__
// ) {
//     devtools = window.__REDUX_DEVTOOLS_EXTENSION__()
// }

// export const Store = createStore(
//     RootReducer,
//     initialState,
//     compose(applyMiddleware(...middlewares), devtools)
// )
