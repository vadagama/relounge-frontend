import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const PREFIX = 'http://localhost:5001/'

export const getCountries = createAsyncThunk(
    'libraries/getCountries',
    async () => {
        const response = await axios.get(`${PREFIX}countries`)
        if (!response) {
            throw new Error("Server Error! Can't get countries.")
        }
        return response.data
    }
)

export const getCities = createAsyncThunk('libraries/getCities', async () => {
    const response = await axios.get(`${PREFIX}cities`)
    if (!response) {
        throw new Error("Server Error! Can't get cities.")
    }
    return response.data
})

export const getCompetences = createAsyncThunk(
    'libraries/getCompetences',
    async () => {
        const response = await axios.get(`${PREFIX}competences`)
        if (!response) {
            throw new Error("Server Error! Can't get competences.")
        }
        return response.data
    }
)

export const getLocations = createAsyncThunk(
    'libraries/getLocations',
    async () => {
        const response = await axios.get(`${PREFIX}locations`)
        if (!response) {
            throw new Error("Server Error! Can't get locations.")
        }
        return response.data
    }
)

const setError = (state, action) => {
    state.status = 'rejected'
    state.error = action.payload
}

const setLoading = (state) => {
    state.status = 'loading'
    state.error = null
}

const initialState = {
    countries: [],
    cities: [],
    competences: [],
    locations: [],
    status: null,
    error: null,
}

const LibrariesSlice = createSlice({
    name: 'libraries',
    initialState,
    reducers: {},
    extraReducers: {
        // Get countries
        [getCountries.pending]: setLoading,
        [getCountries.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.countries = action.payload
        },
        [getCountries.rejected]: setError,

        // Get cities
        [getCities.pending]: setLoading,
        [getCities.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.cities = action.payload
        },
        [getCities.rejected]: setError,

        // Get competences
        [getCompetences.pending]: setLoading,
        [getCompetences.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            state.competences = action.payload
        },
        [getCompetences.rejected]: setError,

        // Get locations
        [getLocations.pending]: setLoading,
        [getLocations.fulfilled]: (state, action) => {
            state.status = 'resolved'
            state.error = null
            const locationsFiltered = [
                ...new Set(action.payload.map((object) => object.name)),
            ]
            state.locations = locationsFiltered
        },
        [getLocations.rejected]: setError,
    },
})

export default LibrariesSlice.reducer
