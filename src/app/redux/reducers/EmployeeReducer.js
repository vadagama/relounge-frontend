import {
    GET_EMPLOYEE_LIST,
    GET_EMPLOYEE_BY_ID,
    ADD_EMPLOYEE,
    DELETE_EMPLOYEE,
    UPDATE_EMPLOYEE,
} from '../actions/EmployeeActions'

const initialState = {
    employeeList: [],
    employee: {},
}

const EmployeeReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_EMPLOYEE_LIST: {
            return {
                ...state,
                employeeList: [...action.payload],
            }
        }
        default: {
            return {
                ...state,
            }
        }
    }
}

export default EmployeeReducer
