import { Navigate } from 'react-router-dom'
import AuthGuard from 'app/auth/AuthGuard'
import NotFound from 'app/features/sessions/NotFound'
import memberRoutes from 'app/features/members/MembersRoutes'
import acquisitionsRoutes from 'app/features/acquisition/AcquisitionsRoutes'
import settingsRoutes from 'app/features/settings/SettingsRoutes'
import employeesRoutes from 'app/features/employees/EmployeesRoutes'
import dashboardRoutes from 'app/features/dashboard/DashboardRoutes'
import sessionRoutes from 'app/features/sessions/SessionRoutes'
import MatxLayout from '../components/MatxLayout/MatxLayout'
import locationsRoutes from 'app/features/locations/LocationsRoutes'
import appointmentsRoutes from 'app/features/appointments/AppointmentsRoutes'
import productsRoutes from 'app/features/products/ProductsRoutes'
import financeRoutes from 'app/features/finance/FinanceRoutes'

export const AllPages = () => {
    const all_routes = [
        {
            element: (
                <AuthGuard>
                    <MatxLayout />
                </AuthGuard>
            ),
            children: [
                ...dashboardRoutes,
                ...memberRoutes,
                ...acquisitionsRoutes,
                ...locationsRoutes,
                ...employeesRoutes,
                ...appointmentsRoutes,
                ...productsRoutes,
                ...settingsRoutes,
                ...financeRoutes,
            ],
        },
        ...sessionRoutes,
        {
            path: '/',
            element: <Navigate to="appointments" />,
        },
        {
            path: '*',
            element: <NotFound />,
        },
    ]

    return all_routes
}
