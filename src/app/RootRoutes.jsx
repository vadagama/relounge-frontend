import React from 'react'
import { Redirect } from 'react-router-dom'
import DashboardRoutes from './features/dashboard/DashboardRoutes'
import MembersRoutes from './features/members/MembersRoutes'
import AcquisitionsRoutes from './features/acquisition/AcquisitionsRoutes'
import SettingsRoutes from './features/settings/SettingsRoutes'
import AppointmentsRoutes from './features/appointments/AppointmentsRoutes'
import ProductsRoutes from './features/products/ProductsRoutes'
import FinanceRoutes from './features/finance/FinanceRoutes'
import EmployeesRoutes from './features/employees/EmployeesRoutes'

const redirectRoute = [
    {
        path: '/',
        exact: true,
        component: () => <Redirect to="/appointments" />,
    },
]

const errorRoute = [
    {
        component: () => <Redirect to="/session/404" />,
    },
]

const routes = [
    ...redirectRoute,
    ...errorRoute,
    ...DashboardRoutes,
    ...MembersRoutes,
    ...AcquisitionsRoutes,
    ...AppointmentsRoutes,
    ...SettingsRoutes,
    ...ProductsRoutes,
    ...FinanceRoutes,
    ...EmployeesRoutes,
]

export default routes
