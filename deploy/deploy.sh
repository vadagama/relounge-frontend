#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="relounge"

# Building React output
npm install
npm run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/ admin@${DEPLOY_SERVER}:/var/www/html/deploy

echo "Finished copying the build files"